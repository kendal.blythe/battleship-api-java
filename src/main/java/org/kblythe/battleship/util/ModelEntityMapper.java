package org.kblythe.battleship.util;

import java.util.List;
import java.util.stream.Collectors;
import org.kblythe.battleship.entity.CoordinateEntity;
import org.kblythe.battleship.entity.GameEntity;
import org.kblythe.battleship.entity.GamePlayerBombEntity;
import org.kblythe.battleship.entity.GamePlayerEntity;
import org.kblythe.battleship.entity.GamePlayerShipEntity;
import org.kblythe.battleship.entity.GridSizeEntity;
import org.kblythe.battleship.entity.PlayerEntity;
import org.kblythe.battleship.model.BombedCoordinate;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.Ship;

public class ModelEntityMapper {
  public static GameEntity toGame(
      Game game, PlayerEntity yourPlayerEntity, PlayerEntity opponentPlayerEntity) {
    GameEntity gameEntity = new GameEntity();

    Grid yourGrid = game.getYourGrid();
    Grid opponentGrid = game.getOpponentGrid();

    gameEntity.setGridConfigId(yourGrid.getGridConfigId());
    gameEntity.setGridSize(new GridSizeEntity(yourGrid.getSize()));
    gameEntity.getGamePlayers().add(toPlayer(yourGrid, gameEntity, yourPlayerEntity));
    gameEntity.getGamePlayers().add(toPlayer(opponentGrid, gameEntity, opponentPlayerEntity));

    return gameEntity;
  }

  public static GamePlayerEntity toPlayer(
      Grid grid, GameEntity gameEntity, PlayerEntity playerEntity) {
    GamePlayerEntity gamePlayerEntity = new GamePlayerEntity();

    List<GamePlayerShipEntity> shipEntities =
        grid.getShips()
            .stream()
            .map(ship -> toShip(ship, gamePlayerEntity))
            .collect(Collectors.toList());

    gamePlayerEntity.setGame(gameEntity);
    gamePlayerEntity.setPlayer(playerEntity);
    gamePlayerEntity.setPlayerNum(grid.getPlayerNum());
    gamePlayerEntity.setShips(shipEntities);

    return gamePlayerEntity;
  }

  public static GamePlayerShipEntity toShip(Ship ship, GamePlayerEntity gamePlayerEntity) {
    GamePlayerShipEntity shipEntity = new GamePlayerShipEntity();

    Coordinate[] coordinates = ship.getCoordinates().toArray(new Coordinate[0]);

    shipEntity.setId(ship.getId());
    shipEntity.setGamePlayer(gamePlayerEntity);
    shipEntity.setName(ship.getName());
    shipEntity.setLength(ship.getLength());
    shipEntity.setStartCoordinate(new CoordinateEntity(coordinates[0]));
    shipEntity.setEndCoordinate(new CoordinateEntity(coordinates[coordinates.length - 1]));

    return shipEntity;
  }

  public static GamePlayerBombEntity toBomb(
      BombedCoordinate bombedCoordinate, GamePlayerEntity gamePlayerEntity) {
    GamePlayerBombEntity bombEntity = new GamePlayerBombEntity();

    Ship sunkenShip = bombedCoordinate.getSunkenShip();

    bombEntity.setGamePlayer(gamePlayerEntity);
    bombEntity.setCoordinate(bombedCoordinate);
    bombEntity.setHit(bombedCoordinate.isHit());
    bombEntity.setSunkenShip(sunkenShip != null ? toShip(sunkenShip, gamePlayerEntity) : null);

    return bombEntity;
  }
}
