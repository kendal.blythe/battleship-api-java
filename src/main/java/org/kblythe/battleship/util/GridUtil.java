package org.kblythe.battleship.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.Ship;

public class GridUtil {
  public static void randomlyPlaceShips(Grid grid, Random random) {
    HashSet<Coordinate> occupiedCoordinates = new HashSet<>();
    int shipsPlaced = 0;
    int horizontalOrientationCount = 0;

    // place largest ships 1st followed by randomly ordered other ships followed by 1 length ships
    Ship largestShip = grid.getShips().first();
    Set<Ship> largestShips =
        grid.getShips()
            .stream()
            .filter(ship -> ship.getLength() == largestShip.getLength())
            .collect(Collectors.toSet());
    List<Ship> otherShips =
        grid.getShips()
            .stream()
            .filter(ship -> !largestShips.contains(ship) && ship.getLength() != 1)
            .collect(Collectors.toList());
    List<Ship> oneLengthShips =
        grid.getShips()
            .stream()
            .filter(ship -> !largestShips.contains(ship) && ship.getLength() == 1)
            .collect(Collectors.toList());

    Collections.shuffle(otherShips);

    LinkedHashSet<Ship> orderedShips = new LinkedHashSet<>();
    orderedShips.add(largestShip);
    orderedShips.addAll(otherShips);
    orderedShips.addAll(oneLengthShips);

    // place each ship
    boolean allowAdjacentShip = random.nextInt(5) == 0; // 1/5 of grids allow 1 adjacent ship
    boolean hasAdjacentShip = false;
    for (Ship ship : orderedShips) {
      // loop until ship start/end coordinates set
      while (ship.getCoordinates().isEmpty()) {
        // get random orientation (if 1st 2 ships have same orientation, then hard code opposite
        // orientation)
        boolean horizontalOrientation;
        if (shipsPlaced == 2) {
          switch (horizontalOrientationCount) {
            case 0:
              horizontalOrientation = true;
              break;
            case 2:
              horizontalOrientation = false;
              break;
            default:
              horizontalOrientation = random.nextBoolean();
              break;
          }
        } else {
          horizontalOrientation = random.nextBoolean();
        }

        // get random start coordinate
        int startX = random.nextInt(grid.getSize().getX()) + 1;
        int startY = random.nextInt(grid.getSize().getY()) + 1;
        Coordinate startCoordinate = new Coordinate(startX, startY);

        // stop here if start coordinate already occupied
        if (occupiedCoordinates.contains(startCoordinate)) {
          continue;
        }

        SortedSet<Coordinate> coordinates = new TreeSet<>();
        coordinates.add(startCoordinate);

        // determine remaining coordinates
        boolean invalidCoordinate = false;
        if (horizontalOrientation) {
          for (int i = 1; i < ship.getLength(); i++) {
            int x = startX + i;
            int y = startY;

            // stop here if coordinate invalid
            if (x > grid.getSize().getX()) {
              invalidCoordinate = true;
              break;
            }

            // stop here if coordinate occupied
            Coordinate coordinate = new Coordinate(x, y);
            if (occupiedCoordinates.contains(coordinate)) {
              invalidCoordinate = true;
              break;
            }

            // add coordinate
            coordinates.add(coordinate);
          }
        } else {
          for (int i = 1; i < ship.getLength(); i++) {
            int x = startX;
            int y = startY + i;

            // stop here if coordinate invalid
            if (y > grid.getSize().getY()) {
              invalidCoordinate = true;
              break;
            }

            // stop here if coordinate occupied
            Coordinate coordinate = new Coordinate(x, y);
            if (occupiedCoordinates.contains(coordinate)) {
              invalidCoordinate = true;
              break;
            }

            // add coordinate
            coordinates.add(coordinate);
          }
        }

        // stop here if coordinate invalid
        if (invalidCoordinate) {
          continue;
        }

        // allow 1 adjacent ship if adjacent ships allowed; otherwise stop here
        if (isShipAdjacentToOccupiedCoordinates(coordinates, occupiedCoordinates)) {
          if (!allowAdjacentShip || hasAdjacentShip) {
            continue;
          }
          hasAdjacentShip = true;
        }

        // set ship coordinates
        ship.setCoordinates(coordinates);

        // update occupied coordinates set
        occupiedCoordinates.addAll(coordinates);

        // update counts
        shipsPlaced++;
        if (horizontalOrientation) {
          horizontalOrientationCount++;
        }
      }
    }
  }

  public static void printGrid(Grid grid, PrintStream stream) {
    List<Set<Coordinate>> shipCoordinates = new ArrayList<>();
    grid.getShips()
        .forEach(ship -> shipCoordinates.add(new HashSet<Coordinate>(ship.getCoordinates())));
    for (int y = 1; y <= grid.getSize().getY(); y++) {
      for (int x = 1; x <= grid.getSize().getX(); x++) {
        Coordinate coordinate = new Coordinate(x, y);
        int shipNum = 0;
        for (int i = 0; i < shipCoordinates.size(); i++) {
          Set<Coordinate> coordinateSet = shipCoordinates.get(i);
          if (coordinateSet.contains(coordinate)) {
            shipNum = i + 1;
            break;
          }
        }
        stream.print(shipNum > 0 ? shipNum : ".");
      }
      stream.println();
    }
    stream.println();
  }

  public static boolean isAdjacentCoordinates(Coordinate coordinate1, Coordinate coordinate2) {
    int diffX = Math.abs(coordinate1.getX() - coordinate2.getX());
    int diffY = Math.abs(coordinate1.getY() - coordinate2.getY());
    return (diffX == 0 && diffY == 1) || (diffX == 1 && diffY == 0);
  }

  public static boolean isShipAdjacentToOccupiedCoordinates(
      Set<Coordinate> shipCoordinates, Set<Coordinate> occupiedCoordinates) {
    for (Coordinate shipCoordinate : shipCoordinates) {
      int x = shipCoordinate.getX();
      int y = shipCoordinate.getY();
      Coordinate top = new Coordinate(x, y - 1);
      Coordinate bottom = new Coordinate(x, y + 1);
      Coordinate left = new Coordinate(x - 1, y);
      Coordinate right = new Coordinate(x + 1, y);
      if (Stream.of(top, bottom, left, right)
          .anyMatch(coordinate -> occupiedCoordinates.contains(coordinate))) {
        return true;
      }
    }
    return false;
  }
}
