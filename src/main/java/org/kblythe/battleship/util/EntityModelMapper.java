package org.kblythe.battleship.util;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.kblythe.battleship.entity.CoordinateEntity;
import org.kblythe.battleship.entity.GameEntity;
import org.kblythe.battleship.entity.GamePlayerBombEntity;
import org.kblythe.battleship.entity.GamePlayerEntity;
import org.kblythe.battleship.entity.GamePlayerShipEntity;
import org.kblythe.battleship.entity.PlayerEntity;
import org.kblythe.battleship.model.BombedCoordinate;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridSize;
import org.kblythe.battleship.model.Ship;

public class EntityModelMapper {
  public static Game toGame(
      GameEntity gameEntity,
      GamePlayerEntity yourGamePlayerEntity,
      GamePlayerEntity opponentGamePlayerEntity) {
    Game game = new Game();

    Grid yourGrid = toGrid(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity.getBombs());
    Grid opponentGrid =
        toGrid(gameEntity, opponentGamePlayerEntity, yourGamePlayerEntity.getBombs());

    game.setId(gameEntity.getId());
    game.setYourGrid(yourGrid);
    game.setOpponentGrid(opponentGrid);
    game.setWinningPlayerNum(gameEntity.getWinningPlayerNum());

    return game;
  }

  public static Grid toGrid(
      GameEntity gameEntity,
      GamePlayerEntity gamePlayerEntity,
      List<GamePlayerBombEntity> bombEntities) {
    Grid grid = new Grid();

    List<Ship> ships =
        gamePlayerEntity
            .getShips()
            .stream()
            .map(shipEntity -> toShip(shipEntity))
            .collect(Collectors.toList());

    List<BombedCoordinate> bombedCoordinates =
        bombEntities
            .stream()
            .map(bombEntity -> toBombedCoordinate(bombEntity))
            .collect(Collectors.toList());

    grid.setGridConfigId(gameEntity.getGridConfigId());
    grid.setSize(new GridSize(gameEntity.getGridSize()));
    grid.setShips(new TreeSet<>(ships));
    grid.setPlayerId(gamePlayerEntity.getId());
    grid.setPlayerNum(gamePlayerEntity.getPlayerNum());
    grid.setSystemPlayer(PlayerEntity.ID_SYSTEM.equals(gamePlayerEntity.getPlayer().getId()));
    grid.setBombedCoordinates(new LinkedHashSet<>(bombedCoordinates));

    return grid;
  }

  public static Ship toShip(GamePlayerShipEntity shipEntity) {
    Ship ship = new Ship();

    ship.setId(shipEntity.getId());
    ship.setName(shipEntity.getName());
    ship.setLength(shipEntity.getLength());

    CoordinateEntity startCoordinate = shipEntity.getStartCoordinate();
    CoordinateEntity endCoordinate = shipEntity.getEndCoordinate();
    if (startCoordinate.getX() == endCoordinate.getX()) {
      for (int y = startCoordinate.getY(); y <= endCoordinate.getY(); y++) {
        ship.getCoordinates().add(new Coordinate(startCoordinate.getX(), y));
      }
    } else {
      for (int x = startCoordinate.getX(); x <= endCoordinate.getX(); x++) {
        ship.getCoordinates().add(new Coordinate(x, startCoordinate.getY()));
      }
    }

    return ship;
  }

  public static BombedCoordinate toBombedCoordinate(GamePlayerBombEntity bombEntity) {
    BombedCoordinate bombedCoordinate = new BombedCoordinate(bombEntity.getCoordinate());

    GamePlayerShipEntity shipEntity = bombEntity.getSunkenShip();

    bombedCoordinate.setHit(bombEntity.isHit());
    bombedCoordinate.setSunkenShip(shipEntity != null ? toShip(shipEntity) : null);

    return bombedCoordinate;
  }
}
