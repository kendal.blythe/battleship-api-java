package org.kblythe.battleship.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.kblythe.battleship.entity.GameEntity;
import org.kblythe.battleship.entity.GamePlayerEntity;
import org.kblythe.battleship.exception.GameOverBombDropException;
import org.kblythe.battleship.exception.PlayerNotFoundException;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.Ship;

public class GameUtil {
  public static GamePlayerEntity[] getGamePlayerEntities(
      GameEntity gameEntity, String gamePlayerId) {
    GamePlayerEntity[] gamePlayerEntities =
        gameEntity.getGamePlayers().toArray(new GamePlayerEntity[0]);

    // return specified game player as 1st array element
    if (gamePlayerEntities[0].getId().equals(gamePlayerId)) {
      return gamePlayerEntities;
    }
    if (gamePlayerEntities[1].getId().equals(gamePlayerId)) {
      GamePlayerEntity tempGamePlayerEntity = gamePlayerEntities[0];
      gamePlayerEntities[0] = gamePlayerEntities[1];
      gamePlayerEntities[1] = tempGamePlayerEntity;
      return gamePlayerEntities;
    }

    throw new PlayerNotFoundException(gamePlayerId);
  }

  public static Coordinate getRandomAvailableCoordinate(Grid grid, Random random) {
    List<Coordinate> availableCoordinates = getAvailableCoordinates(grid);
    if (availableCoordinates.size() == 0) {
      throw new GameOverBombDropException();
    }
    Collections.shuffle(availableCoordinates, random);
    return availableCoordinates.get(random.nextInt(availableCoordinates.size()));
  }

  public static List<Coordinate> getAvailableCoordinates(Grid grid) {
    List<Coordinate> availableCoordinates = new ArrayList<>();
    for (int x = 1; x <= grid.getSize().getX(); x++) {
      for (int y = 1; y <= grid.getSize().getY(); y++) {
        Coordinate coordinate = new Coordinate(x, y);
        if (!grid.getBombedCoordinates().contains(coordinate)) {
          availableCoordinates.add(coordinate);
        }
      }
    }
    return availableCoordinates;
  }

  public static boolean isAllShipsSunk(Grid grid) {
    int sunkenShipCount = 0;
    for (Ship ship : grid.getShips()) {
      if (isSunkenShip(ship, grid)) {
        sunkenShipCount++;
      }
    }
    return sunkenShipCount == grid.getShips().size();
  }

  public static boolean isSunkenShip(Ship ship, Grid grid) {
    int hitCount = 0;
    for (Coordinate coordinate : ship.getCoordinates()) {
      if (grid.getBombedCoordinates().contains(coordinate)) {
        if (++hitCount == ship.getLength()) {
          return true;
        }
      }
    }
    return false;
  }
}
