package org.kblythe.battleship.repository;

import org.kblythe.battleship.entity.GameEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<GameEntity, String> {}
