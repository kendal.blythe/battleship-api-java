package org.kblythe.battleship.l10n;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import org.springframework.context.i18n.LocaleContextHolder;

public class Errors {
  private static final String BUNDLE_BASE_NAME = "l10n.errors";
  private static final ResourceBundle DEFAULT_BUNDLE = ResourceBundle.getBundle(BUNDLE_BASE_NAME);

  public static String getMessage(String key) {
    return DEFAULT_BUNDLE.getString(key);
  }

  public static String getMessage(String key, Object... params) {
    return MessageFormat.format(DEFAULT_BUNDLE.getString(key), params);
  }

  public static String getLocalizedMessage(String key) {
    return getLocalizedBundle().getString(key);
  }

  public static String getLocalizedMessage(String key, Object... params) {
    return MessageFormat.format(getLocalizedBundle().getString(key), params);
  }

  public static ResourceBundle getLocalizedBundle() {
    return ResourceBundle.getBundle(BUNDLE_BASE_NAME, LocaleContextHolder.getLocale());
  }
}
