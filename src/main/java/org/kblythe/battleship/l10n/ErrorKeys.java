package org.kblythe.battleship.l10n;

public interface ErrorKeys {
  String GRID_CONFIG_NOT_FOUND = "gridConfigNotFound.fmt";
  String GAME_NOT_FOUND = "gameNotFound.fmt";
  String PLAYER_NOT_FOUND = "playerNotFound.fmt";
  String GRID_CONFIG_MISMATCH = "gridConfigMismatch.fmt";
  String INVALID_SHIP_COORDINATE = "invalidShipCoordinate.fmt";
  String OVERLAPPING_SHIP_COORDINATE = "overlappingShipCoordinate.fmt";
  String NONADJACENT_SHIP_COORDINATES = "nonadjacentShipCoordinates.fmt";
  String GAME_OVER_BOMB_DROP = "gameOverBombDrop.txt";
  String NOT_TURN_BOMB_DROP = "notTurnBombDrop.txt";
  String INVALID_BOMB_COORDINATE = "invalidBombCoordinate.fmt";
  String COORDINATE_ALREADY_BOMBED = "coordinateAlreadyBombed.fmt";
}
