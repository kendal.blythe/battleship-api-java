package org.kblythe.battleship.model;

import java.util.ArrayList;
import java.util.List;

public class AppConfig {
  private List<GridConfig> gridConfigs = new ArrayList<>();

  public List<GridConfig> getGridConfigs() {
    return gridConfigs;
  }

  public void setGridConfigs(List<GridConfig> gridConfigs) {
    this.gridConfigs = gridConfigs;
  }
}
