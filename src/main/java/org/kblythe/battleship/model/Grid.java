package org.kblythe.battleship.model;

import java.util.LinkedHashSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class Grid {
  private String gridConfigId;
  private GridSize size;
  private SortedSet<Ship> ships = new TreeSet<>();
  private String playerId;
  private int playerNum;
  private boolean systemPlayer;
  private LinkedHashSet<BombedCoordinate> bombedCoordinates = new LinkedHashSet<>();

  public Grid() {}

  public Grid(GridConfig gridConfig) {
    setGridConfigId(gridConfig.getId());
    setSize(new GridSize(gridConfig.getSize()));
    gridConfig.getShips().forEach(ship -> getShips().add(new Ship(ship)));
  }

  public String getGridConfigId() {
    return gridConfigId;
  }

  public void setGridConfigId(String gridConfigId) {
    this.gridConfigId = gridConfigId;
  }

  public GridSize getSize() {
    return size;
  }

  public void setSize(GridSize size) {
    this.size = size;
  }

  public SortedSet<Ship> getShips() {
    return ships;
  }

  public void setShips(SortedSet<Ship> ships) {
    this.ships = ships;
  }

  public String getPlayerId() {
    return playerId;
  }

  public void setPlayerId(String playerId) {
    this.playerId = playerId;
  }

  public int getPlayerNum() {
    return playerNum;
  }

  public void setPlayerNum(int playerNum) {
    this.playerNum = playerNum;
  }

  public boolean isSystemPlayer() {
    return systemPlayer;
  }

  public void setSystemPlayer(boolean systemPlayer) {
    this.systemPlayer = systemPlayer;
  }

  public LinkedHashSet<BombedCoordinate> getBombedCoordinates() {
    return bombedCoordinates;
  }

  public void setBombedCoordinates(LinkedHashSet<BombedCoordinate> bombedCoordinates) {
    this.bombedCoordinates = bombedCoordinates;
  }
}
