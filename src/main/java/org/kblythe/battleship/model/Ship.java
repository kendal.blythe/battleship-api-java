package org.kblythe.battleship.model;

import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;

public class Ship implements Comparable<Ship> {
  private String id;
  private String name;
  private int length;
  private SortedSet<Coordinate> coordinates = new TreeSet<>();

  public Ship() {}

  public Ship(String name, int length) {
    setName(name);
    setLength(length);
  }

  public Ship(Ship ship) {
    this(ship.getName(), ship.getLength());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public SortedSet<Coordinate> getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(SortedSet<Coordinate> coordinates) {
    this.coordinates = coordinates;
  }

  @Override
  public int compareTo(Ship obj) {
    int rv = obj.getLength() - this.getLength(); // 1 - compare length descending
    return (rv == 0)
        ? StringUtils.compareIgnoreCase(this.getName(), obj.getName()) // 2 - compare name ascending
        : rv;
  }
}
