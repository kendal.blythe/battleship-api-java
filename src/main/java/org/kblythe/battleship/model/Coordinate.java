package org.kblythe.battleship.model;

import org.kblythe.battleship.entity.CoordinateEntity;

public class Coordinate extends CoordinateEntity implements Comparable<Coordinate> {
  public Coordinate() {}

  public Coordinate(int x, int y) {
    super(x, y);
  }

  public Coordinate(CoordinateEntity coordinate) {
    super(coordinate);
  }

  @Override
  public int compareTo(Coordinate obj) {
    int thisVal = this.getX() + this.getY();
    int thatVal = obj.getX() + obj.getY();
    return thisVal - thatVal;
  }

  @Override
  public String toString() {
    return String.format("(%d,%d)", getX(), getY());
  }
}
