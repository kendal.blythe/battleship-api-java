package org.kblythe.battleship.model;

public class Game {
  private String id;
  private Grid yourGrid;
  private Grid opponentGrid;
  private Integer winningPlayerNum;

  public Game() {}

  public Game(Grid yourGrid, Grid opponentGrid) {
    setYourGrid(yourGrid);
    setOpponentGrid(opponentGrid);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Grid getYourGrid() {
    return yourGrid;
  }

  public void setYourGrid(Grid yourGrid) {
    this.yourGrid = yourGrid;
  }

  public Grid getOpponentGrid() {
    return opponentGrid;
  }

  public void setOpponentGrid(Grid opponentGrid) {
    this.opponentGrid = opponentGrid;
  }

  public Integer getWinningPlayerNum() {
    return winningPlayerNum;
  }

  public void setWinningPlayerNum(Integer winningPlayerNum) {
    this.winningPlayerNum = winningPlayerNum;
  }
}
