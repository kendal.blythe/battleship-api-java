package org.kblythe.battleship.model;

import java.util.SortedSet;
import java.util.TreeSet;

public class GridConfig {
  private String id;
  private String name;
  private GridSize size;
  private SortedSet<Ship> ships = new TreeSet<>();

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GridSize getSize() {
    return size;
  }

  public void setSize(GridSize size) {
    this.size = size;
  }

  public SortedSet<Ship> getShips() {
    return ships;
  }

  public void setShips(SortedSet<Ship> ships) {
    this.ships = ships;
  }
}
