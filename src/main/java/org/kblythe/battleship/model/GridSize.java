package org.kblythe.battleship.model;

import org.kblythe.battleship.entity.GridSizeEntity;

public class GridSize extends GridSizeEntity implements Comparable<GridSize> {
  public GridSize() {}

  public GridSize(int x, int y) {
    super(x, y);
  }

  public GridSize(GridSizeEntity gridSize) {
    super(gridSize);
  }

  @Override
  public int compareTo(GridSize obj) {
    int thisVal = this.getX() + this.getY();
    int thatVal = obj.getX() + obj.getY();
    return thisVal - thatVal;
  }

  @Override
  public String toString() {
    return String.format("%dx%d", getX(), getY());
  }
}
