package org.kblythe.battleship.model;

import org.kblythe.battleship.entity.CoordinateEntity;

public class BombedCoordinate extends Coordinate {
  private boolean hit;
  private Ship sunkenShip;

  public BombedCoordinate() {}

  public BombedCoordinate(CoordinateEntity coordinate) {
    super(coordinate);
  }

  public boolean isHit() {
    return hit;
  }

  public void setHit(boolean hit) {
    this.hit = hit;
  }

  public Ship getSunkenShip() {
    return sunkenShip;
  }

  public void setSunkenShip(Ship sunkenShip) {
    this.sunkenShip = sunkenShip;
  }
}
