package org.kblythe.battleship.entity;

import java.util.Objects;

public abstract class AbstractIdentifiableEntity<T> {
  public abstract T getId();

  public abstract void setId(T id);

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  @SuppressWarnings("unchecked")
  public boolean equals(Object obj) {
    if (!(obj instanceof AbstractIdentifiableEntity)) {
      return false;
    }
    AbstractIdentifiableEntity<T> that = (AbstractIdentifiableEntity<T>) obj;
    return Objects.equals(this.getId(), that.getId());
  }
}
