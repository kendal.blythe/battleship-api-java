package org.kblythe.battleship.entity;

import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class CoordinateEntity {
  private int x;
  private int y;

  public CoordinateEntity() {}

  public CoordinateEntity(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public CoordinateEntity(CoordinateEntity coordinate) {
    this(coordinate.getX(), coordinate.getY());
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof CoordinateEntity)) {
      return false;
    }
    CoordinateEntity that = (CoordinateEntity) obj;
    return Objects.equals(this.x, that.x) && Objects.equals(this.y, that.y);
  }
}
