package org.kblythe.battleship.entity;

import java.util.Date;
import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "game_player_bomb")
@EntityListeners(AuditingEntityListener.class)
public class GamePlayerBombEntity extends AbstractIdentifiableEntity<String> {
  @Id
  @Column(name = "game_player_bomb_id")
  private String id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "game_player_id")
  private GamePlayerEntity gamePlayer;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "x", column = @Column(name = "coord_x")),
    @AttributeOverride(name = "y", column = @Column(name = "coord_y"))
  })
  private CoordinateEntity coordinate;

  @Column(name = "hit_flg")
  private boolean hit;

  @OneToOne
  @JoinColumn(name = "sunken_game_player_ship_id")
  private GamePlayerShipEntity sunkenShip;

  @CreatedDate
  @Column(name = "created_date")
  private Date createdDate;

  @PrePersist
  public void prePersist() {
    id = UUID.randomUUID().toString();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public GamePlayerEntity getGamePlayer() {
    return gamePlayer;
  }

  public void setGamePlayer(GamePlayerEntity gamePlayer) {
    this.gamePlayer = gamePlayer;
  }

  public CoordinateEntity getCoordinate() {
    return coordinate;
  }

  public void setCoordinate(CoordinateEntity coordinate) {
    this.coordinate = coordinate;
  }

  public boolean isHit() {
    return hit;
  }

  public void setHit(boolean hit) {
    this.hit = hit;
  }

  public GamePlayerShipEntity getSunkenShip() {
    return sunkenShip;
  }

  public void setSunkenShip(GamePlayerShipEntity sunkenShip) {
    this.sunkenShip = sunkenShip;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
}
