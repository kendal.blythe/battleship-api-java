package org.kblythe.battleship.entity;

import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "game_player_ship")
public class GamePlayerShipEntity extends AbstractIdentifiableEntity<String> {
  @Id
  @Column(name = "game_player_ship_id")
  private String id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "game_player_id")
  private GamePlayerEntity gamePlayer;

  @Column(name = "ship_nm")
  private String name;

  @Column(name = "ship_len")
  private int length;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "x", column = @Column(name = "start_coord_x")),
    @AttributeOverride(name = "y", column = @Column(name = "start_coord_y"))
  })
  private CoordinateEntity startCoordinate;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "x", column = @Column(name = "end_coord_x")),
    @AttributeOverride(name = "y", column = @Column(name = "end_coord_y"))
  })
  private CoordinateEntity endCoordinate;

  @PrePersist
  public void prePersist() {
    id = UUID.randomUUID().toString();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public GamePlayerEntity getGamePlayer() {
    return gamePlayer;
  }

  public void setGamePlayer(GamePlayerEntity gamePlayer) {
    this.gamePlayer = gamePlayer;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public CoordinateEntity getStartCoordinate() {
    return startCoordinate;
  }

  public void setStartCoordinate(CoordinateEntity startCoordinate) {
    this.startCoordinate = startCoordinate;
  }

  public CoordinateEntity getEndCoordinate() {
    return endCoordinate;
  }

  public void setEndCoordinate(CoordinateEntity endCoordinate) {
    this.endCoordinate = endCoordinate;
  }
}
