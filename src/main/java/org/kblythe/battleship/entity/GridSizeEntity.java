package org.kblythe.battleship.entity;

import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class GridSizeEntity {
  private int x;
  private int y;

  public GridSizeEntity() {}

  public GridSizeEntity(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public GridSizeEntity(GridSizeEntity gridSize) {
    this(gridSize.getX(), gridSize.getY());
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GridSizeEntity)) {
      return false;
    }
    GridSizeEntity that = (GridSizeEntity) obj;
    return Objects.equals(this.x, that.x) && Objects.equals(this.y, that.y);
  }
}
