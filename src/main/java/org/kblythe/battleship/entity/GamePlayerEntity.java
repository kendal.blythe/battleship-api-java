package org.kblythe.battleship.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "game_player")
public class GamePlayerEntity extends AbstractIdentifiableEntity<String> {
  @Id
  @Column(name = "game_player_id")
  private String id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "game_id")
  private GameEntity game;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "player_id")
  private PlayerEntity player;

  @Column(name = "player_num")
  private int playerNum;

  @OneToMany(mappedBy = "gamePlayer", cascade = CascadeType.ALL, orphanRemoval = true)
  @OrderBy("length desc, name asc")
  private List<GamePlayerShipEntity> ships = new ArrayList<>();

  @OneToMany(mappedBy = "gamePlayer", cascade = CascadeType.ALL, orphanRemoval = true)
  @OrderBy("createdDate asc")
  private List<GamePlayerBombEntity> bombs = new ArrayList<>();

  @PrePersist
  public void prePersist() {
    id = UUID.randomUUID().toString();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public GameEntity getGame() {
    return game;
  }

  public void setGame(GameEntity game) {
    this.game = game;
  }

  public PlayerEntity getPlayer() {
    return player;
  }

  public void setPlayer(PlayerEntity player) {
    this.player = player;
  }

  public int getPlayerNum() {
    return playerNum;
  }

  public void setPlayerNum(int playerNum) {
    this.playerNum = playerNum;
  }

  public List<GamePlayerShipEntity> getShips() {
    return ships;
  }

  public void setShips(List<GamePlayerShipEntity> ships) {
    this.ships = ships;
  }

  public List<GamePlayerBombEntity> getBombs() {
    return bombs;
  }

  public void setBombs(List<GamePlayerBombEntity> bombs) {
    this.bombs = bombs;
  }
}
