package org.kblythe.battleship.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "game")
@EntityListeners(AuditingEntityListener.class)
public class GameEntity extends AbstractIdentifiableEntity<String> {
  @Id
  @Column(name = "game_id")
  private String id;

  @Column(name = "grid_config_id")
  private String gridConfigId;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "x", column = @Column(name = "grid_size_x")),
    @AttributeOverride(name = "y", column = @Column(name = "grid_size_y"))
  })
  private GridSizeEntity gridSize;

  @OneToMany(mappedBy = "game", cascade = CascadeType.ALL, orphanRemoval = true)
  @OrderBy("playerNum asc")
  private List<GamePlayerEntity> gamePlayers = new ArrayList<>();

  @Column(name = "turn_player_num")
  private Integer turnPlayerNum;

  @Column(name = "winning_player_num")
  private Integer winningPlayerNum;

  @CreatedDate
  @Column(name = "created_date")
  private Date createdDate;

  @LastModifiedDate
  @Column(name = "last_modified_date")
  private Date lastModifiedDate;

  @PrePersist
  public void prePersist() {
    id = UUID.randomUUID().toString();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getGridConfigId() {
    return gridConfigId;
  }

  public void setGridConfigId(String gridConfigId) {
    this.gridConfigId = gridConfigId;
  }

  public GridSizeEntity getGridSize() {
    return gridSize;
  }

  public void setGridSize(GridSizeEntity gridSize) {
    this.gridSize = gridSize;
  }

  public List<GamePlayerEntity> getGamePlayers() {
    return gamePlayers;
  }

  public void setGamePlayers(List<GamePlayerEntity> gamePlayers) {
    this.gamePlayers = gamePlayers;
  }

  public Integer getTurnPlayerNum() {
    return turnPlayerNum;
  }

  public void setTurnPlayerNum(Integer turnPlayerNum) {
    this.turnPlayerNum = turnPlayerNum;
  }

  public Integer getWinningPlayerNum() {
    return winningPlayerNum;
  }

  public void setWinningPlayerNum(Integer winningPlayerNum) {
    this.winningPlayerNum = winningPlayerNum;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Date lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }
}
