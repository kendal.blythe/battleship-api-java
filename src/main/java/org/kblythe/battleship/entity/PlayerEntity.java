package org.kblythe.battleship.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "player")
public class PlayerEntity extends AbstractIdentifiableEntity<String> {
  public static final String ID_SYSTEM = "_SYSTEM_";
  public static final String ID_ANONYMOUS = "_ANONYMOUS_";

  public PlayerEntity() {}

  public PlayerEntity(String id, String name) {
    super();
    this.id = id;
    this.name = name;
  }

  @Id
  @Column(name = "player_id")
  private String id;

  @Column(name = "player_nm")
  private String name;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
