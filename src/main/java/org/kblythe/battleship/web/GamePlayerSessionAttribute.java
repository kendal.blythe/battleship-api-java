package org.kblythe.battleship.web;

import org.kblythe.battleship.model.Game;

public class GamePlayerSessionAttribute {
  public static final String NAME = "org.kblythe.battleship.gamePlayer";

  private String gameId;
  private String gamePlayerId;

  public GamePlayerSessionAttribute(String gameId, String gamePlayerId) {
    this.gameId = gameId;
    this.gamePlayerId = gamePlayerId;
  }

  public GamePlayerSessionAttribute(Game game) {
    this(game.getId(), game.getYourGrid().getPlayerId());
  }

  public String getGameId() {
    return gameId;
  }

  public String getGamePlayerId() {
    return gamePlayerId;
  }
}
