package org.kblythe.battleship.graphql;

import javax.servlet.http.HttpSession;
import graphql.schema.DataFetchingEnvironment;
import graphql.servlet.GraphQLContext;

public abstract class AbstractResolver {
  protected void setSessionAttribute(String name, Object value, DataFetchingEnvironment env) {
    HttpSession session = getHttpSession(env);
    if (session != null) {
      session.setAttribute(name, value);
    }
  }

  protected Object getSessionAttribute(String name, DataFetchingEnvironment env) {
    HttpSession session = getHttpSession(env);
    return (session != null) ? session.getAttribute(name) : null;
  }

  protected HttpSession getHttpSession(DataFetchingEnvironment env) {
    HttpSession session = null;
    GraphQLContext ctx = env.getContext();
    if (ctx.getHttpServletRequest().isPresent()) {
      session = ctx.getHttpServletRequest().get().getSession();
    }
    return session;
  }
}
