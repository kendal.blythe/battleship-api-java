package org.kblythe.battleship.graphql;

import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.service.GameService;
import org.kblythe.battleship.web.GamePlayerSessionAttribute;
import org.springframework.stereotype.Component;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import graphql.schema.DataFetchingEnvironment;

@Component
public class MutationResolver extends AbstractResolver implements GraphQLMutationResolver {
  private ConfigService configService;
  private GameService gameService;

  public MutationResolver(ConfigService configService, GameService gameService) {
    this.configService = configService;
    this.gameService = gameService;
  }

  public Grid createGrid(String gridConfigId) {
    return configService.createGrid(gridConfigId);
  }

  public Game startGame(Grid grid, DataFetchingEnvironment env) {
    Game game = gameService.startGame(grid);
    setSessionAttribute(GamePlayerSessionAttribute.NAME, new GamePlayerSessionAttribute(game), env);
    return game;
  }

  public Game dropBomb(
      String gameId, String playerId, Coordinate coordinate, DataFetchingEnvironment env) {
    Game game = gameService.dropBomb(gameId, playerId, coordinate);
    setSessionAttribute(GamePlayerSessionAttribute.NAME, new GamePlayerSessionAttribute(game), env);
    return game;
  }

  public boolean endGame(String gameId, String playerId, DataFetchingEnvironment env) {
    gameService.endGame(gameId, playerId);
    GamePlayerSessionAttribute gamePlayerAttr =
        (GamePlayerSessionAttribute) getSessionAttribute(GamePlayerSessionAttribute.NAME, env);
    if (gamePlayerAttr != null && gamePlayerAttr.getGameId().equals(gameId)) {
      setSessionAttribute(GamePlayerSessionAttribute.NAME, null, env);
    }
    return true;
  }
}
