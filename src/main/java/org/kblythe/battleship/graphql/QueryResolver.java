package org.kblythe.battleship.graphql;

import java.util.Collection;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.service.GameService;
import org.kblythe.battleship.web.GamePlayerSessionAttribute;
import org.springframework.stereotype.Component;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.schema.DataFetchingEnvironment;

@Component
public class QueryResolver extends AbstractResolver implements GraphQLQueryResolver {
  private ConfigService configService;
  private GameService gameService;

  public QueryResolver(ConfigService configService, GameService gameService) {
    this.configService = configService;
    this.gameService = gameService;
  }

  public Collection<GridConfig> getGridConfigs() {
    return configService.getGridConfigs();
  }

  public GridConfig getGridConfig(String gridConfigId) {
    return configService.getGridConfig(gridConfigId);
  }

  public Game getGame(String gameId, String playerId) {
    return gameService.getGame(gameId, playerId);
  }

  public Game getCurrentGame(DataFetchingEnvironment env) {
    GamePlayerSessionAttribute attr =
        (GamePlayerSessionAttribute) getSessionAttribute(GamePlayerSessionAttribute.NAME, env);
    return attr != null ? gameService.getGame(attr.getGameId(), attr.getGamePlayerId()) : null;
  }
}
