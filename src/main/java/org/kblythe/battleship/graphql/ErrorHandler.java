package org.kblythe.battleship.graphql;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.servlet.GenericGraphQLError;
import graphql.servlet.GraphQLErrorHandler;

@Component
public class ErrorHandler implements GraphQLErrorHandler {
  @Override
  public List<GraphQLError> processErrors(List<GraphQLError> errors) {
    return errors
        .stream()
        .map(
            error -> {
              String message =
                  (error instanceof ExceptionWhileDataFetching)
                      ? ((ExceptionWhileDataFetching) error).getException().getLocalizedMessage()
                      : error.getMessage();
              return new GenericGraphQLError(message);
            })
        .collect(Collectors.toList());
  }
}
