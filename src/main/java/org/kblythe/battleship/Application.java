package org.kblythe.battleship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
@EnableCaching(proxyTargetClass = true)
public class Application {
  public static void main(String... args) {
    SpringApplication.run(Application.class, args);
  }
}
