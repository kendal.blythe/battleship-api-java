package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;

public class NotTurnBombDropException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public NotTurnBombDropException() {
    super(
        ErrorCodes.NOT_TURN_BOMB_DROP,
        Errors.getMessage(ErrorKeys.NOT_TURN_BOMB_DROP),
        Errors.getLocalizedMessage(ErrorKeys.NOT_TURN_BOMB_DROP));
  }
}
