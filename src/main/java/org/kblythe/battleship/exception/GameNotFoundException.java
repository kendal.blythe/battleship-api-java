package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;

public class GameNotFoundException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public GameNotFoundException(String id) {
    super(
        ErrorCodes.GAME_NOT_FOUND,
        Errors.getMessage(ErrorKeys.GAME_NOT_FOUND, id),
        Errors.getLocalizedMessage(ErrorKeys.GAME_NOT_FOUND, id));
  }
}
