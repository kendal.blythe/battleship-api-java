package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;

public class InvalidBombCoordinateException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public InvalidBombCoordinateException(Coordinate coordinate) {
    super(
        ErrorCodes.INVALID_BOMB_COORDINATE,
        Errors.getMessage(ErrorKeys.INVALID_BOMB_COORDINATE, coordinate),
        Errors.getLocalizedMessage(ErrorKeys.INVALID_BOMB_COORDINATE, coordinate));
  }
}
