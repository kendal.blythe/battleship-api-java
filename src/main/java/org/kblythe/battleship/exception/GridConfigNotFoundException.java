package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;

public class GridConfigNotFoundException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public GridConfigNotFoundException(String id) {
    super(
        ErrorCodes.GRID_CONFIG_NOT_FOUND,
        Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, id),
        Errors.getLocalizedMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, id));
  }
}
