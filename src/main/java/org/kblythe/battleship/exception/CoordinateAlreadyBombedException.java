package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;

public class CoordinateAlreadyBombedException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public CoordinateAlreadyBombedException(Coordinate coordinate) {
    super(
        ErrorCodes.COORDINATE_ALREADY_BOMBED,
        Errors.getMessage(ErrorKeys.COORDINATE_ALREADY_BOMBED, coordinate),
        Errors.getLocalizedMessage(ErrorKeys.COORDINATE_ALREADY_BOMBED, coordinate));
  }
}
