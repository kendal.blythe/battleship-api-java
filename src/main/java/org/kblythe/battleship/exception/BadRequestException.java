package org.kblythe.battleship.exception;

public abstract class BadRequestException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  private String localizedMessage;

  public BadRequestException(int errorCode, String message, String localizedMessage) {
    super(getMessageWithErrorCode(errorCode, message));
    this.localizedMessage = getMessageWithErrorCode(errorCode, localizedMessage);
  }

  @Override
  public String getLocalizedMessage() {
    return localizedMessage;
  }

  protected static String getMessageWithErrorCode(int errorCode, String message) {
    return String.format("%d-%s", errorCode, message);
  }
}
