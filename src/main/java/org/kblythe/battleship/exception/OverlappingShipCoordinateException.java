package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;

public class OverlappingShipCoordinateException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public OverlappingShipCoordinateException(Coordinate coordinate) {
    super(
        ErrorCodes.OVERLAPPING_SHIP_COORDINATE,
        Errors.getMessage(ErrorKeys.OVERLAPPING_SHIP_COORDINATE, coordinate),
        Errors.getLocalizedMessage(ErrorKeys.OVERLAPPING_SHIP_COORDINATE, coordinate));
  }
}
