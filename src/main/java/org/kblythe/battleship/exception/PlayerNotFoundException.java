package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;

public class PlayerNotFoundException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public PlayerNotFoundException(String id) {
    super(
        ErrorCodes.PLAYER_NOT_FOUND,
        Errors.getMessage(ErrorKeys.PLAYER_NOT_FOUND, id),
        Errors.getLocalizedMessage(ErrorKeys.PLAYER_NOT_FOUND, id));
  }
}
