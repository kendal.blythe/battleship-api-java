package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;

public class InvalidShipCoordinateException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public InvalidShipCoordinateException(Coordinate coordinate) {
    super(
        ErrorCodes.INVALID_SHIP_COORDINATE,
        Errors.getMessage(ErrorKeys.INVALID_SHIP_COORDINATE, coordinate),
        Errors.getLocalizedMessage(ErrorKeys.INVALID_SHIP_COORDINATE, coordinate));
  }
}
