package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;

public class GameOverBombDropException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public GameOverBombDropException() {
    super(
        ErrorCodes.GAME_OVER_BOMB_DROP,
        Errors.getMessage(ErrorKeys.GAME_OVER_BOMB_DROP),
        Errors.getLocalizedMessage(ErrorKeys.GAME_OVER_BOMB_DROP));
  }
}
