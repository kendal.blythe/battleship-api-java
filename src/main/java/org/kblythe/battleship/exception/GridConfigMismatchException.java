package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.GridConfig;

public class GridConfigMismatchException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public GridConfigMismatchException(GridConfig gridConfig) {
    super(
        ErrorCodes.GRID_CONFIG_MISMATCH,
        Errors.getMessage(ErrorKeys.GRID_CONFIG_MISMATCH, gridConfig.getName()),
        Errors.getLocalizedMessage(ErrorKeys.GRID_CONFIG_MISMATCH, gridConfig.getName()));
  }
}
