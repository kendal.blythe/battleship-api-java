package org.kblythe.battleship.exception;

import org.kblythe.battleship.l10n.ErrorCodes;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;

public class NonadjacentShipCoordinatesException extends BadRequestException {
  private static final long serialVersionUID = 1L;

  public NonadjacentShipCoordinatesException(Coordinate coordinate1, Coordinate coordinate2) {
    super(
        ErrorCodes.NONADJACENT_SHIP_COORDINATES,
        Errors.getMessage(ErrorKeys.NONADJACENT_SHIP_COORDINATES, coordinate1, coordinate2),
        Errors.getLocalizedMessage(
            ErrorKeys.NONADJACENT_SHIP_COORDINATES, coordinate1, coordinate2));
  }
}
