package org.kblythe.battleship.service;

import org.kblythe.battleship.entity.PlayerEntity;
import org.kblythe.battleship.repository.PlayerRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {
  private PlayerRepository playerRepository;

  public PlayerService(PlayerRepository playerRepository) {
    this.playerRepository = playerRepository;
  }

  @Cacheable("systemPlayer")
  public PlayerEntity getSystemPlayer() {
    return playerRepository.findById(PlayerEntity.ID_SYSTEM).get();
  }

  @Cacheable("anonymousPlayer")
  public PlayerEntity getAnonymousPlayer() {
    return playerRepository.findById(PlayerEntity.ID_ANONYMOUS).get();
  }
}
