package org.kblythe.battleship.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.kblythe.battleship.entity.GameEntity;
import org.kblythe.battleship.entity.GamePlayerEntity;
import org.kblythe.battleship.entity.PlayerEntity;
import org.kblythe.battleship.exception.CoordinateAlreadyBombedException;
import org.kblythe.battleship.exception.GameNotFoundException;
import org.kblythe.battleship.exception.GameOverBombDropException;
import org.kblythe.battleship.exception.GridConfigMismatchException;
import org.kblythe.battleship.exception.InvalidBombCoordinateException;
import org.kblythe.battleship.exception.InvalidShipCoordinateException;
import org.kblythe.battleship.exception.NonadjacentShipCoordinatesException;
import org.kblythe.battleship.exception.NotTurnBombDropException;
import org.kblythe.battleship.exception.OverlappingShipCoordinateException;
import org.kblythe.battleship.model.BombedCoordinate;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.model.Ship;
import org.kblythe.battleship.repository.GameRepository;
import org.kblythe.battleship.util.EntityModelMapper;
import org.kblythe.battleship.util.GameUtil;
import org.kblythe.battleship.util.GridUtil;
import org.kblythe.battleship.util.ModelEntityMapper;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class GameService {
  private ConfigService configService;
  private PlayerService playerService;
  private GameRepository gameRepository;
  private Random random = new Random();

  public GameService(
      ConfigService configService, PlayerService playerService, GameRepository gameRepository) {
    this.configService = configService;
    this.playerService = playerService;
    this.gameRepository = gameRepository;
  }

  public void setRandom(Random random) {
    this.random = random;
  }

  public Game getGame(String gameId, String gamePlayerId) {
    // get game entity
    GameEntity gameEntity = getGameEntity(gameId);

    // get game player entities
    GamePlayerEntity[] gamePlayerEntities =
        GameUtil.getGamePlayerEntities(gameEntity, gamePlayerId);
    GamePlayerEntity yourGamePlayerEntity = gamePlayerEntities[0];
    GamePlayerEntity opponentGamePlayerEntity = gamePlayerEntities[1];

    // return game
    return EntityModelMapper.toGame(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity);
  }

  public Game startGame(Grid yourGrid) {
    // validate your grid matches the grid configuration
    GridConfig gridConfig = configService.getGridConfig(yourGrid.getGridConfigId());
    if (!gridConfig.getSize().equals(yourGrid.getSize())) {
      throw new GridConfigMismatchException(gridConfig);
    }
    if (gridConfig.getShips().size() != yourGrid.getShips().size()) {
      throw new GridConfigMismatchException(gridConfig);
    }
    Iterator<Ship> configShipIter = gridConfig.getShips().iterator();
    for (Ship yourShip : yourGrid.getShips()) {
      Ship configShip = configShipIter.next();
      if (configShip.compareTo(yourShip) != 0) {
        throw new GridConfigMismatchException(gridConfig);
      }
      if (configShip.getLength() != yourShip.getCoordinates().size()) {
        throw new GridConfigMismatchException(gridConfig);
      }
    }

    // validate your grid ship coordinates
    HashSet<Coordinate> validCoordinates =
        new HashSet<>(GameUtil.getAvailableCoordinates(yourGrid));
    HashSet<Coordinate> placedCoordinates = new HashSet<>();
    for (Ship yourShip : yourGrid.getShips()) {
      Coordinate previousCoordinate = null;
      for (Coordinate coordinate : yourShip.getCoordinates()) {
        if (!validCoordinates.contains(coordinate)) {
          throw new InvalidShipCoordinateException(coordinate);
        }
        if (placedCoordinates.contains(coordinate)) {
          throw new OverlappingShipCoordinateException(coordinate);
        }
        if (previousCoordinate != null
            && !GridUtil.isAdjacentCoordinates(coordinate, previousCoordinate)) {
          throw new NonadjacentShipCoordinatesException(previousCoordinate, coordinate);
        }
        placedCoordinates.add(coordinate);
        previousCoordinate = coordinate;
      }
    }

    // create opponent grid
    Grid opponentGrid = new Grid(gridConfig);
    GridUtil.randomlyPlaceShips(opponentGrid, random);

    // determine 1st/2nd player
    int yourPlayerNum = random.nextBoolean() ? 1 : 2;
    int opponentPlayerNum = yourPlayerNum == 1 ? 2 : 1;

    yourGrid.setPlayerNum(yourPlayerNum);
    opponentGrid.setPlayerNum(opponentPlayerNum);

    // drop system player bomb if opponent 1st player
    if (opponentPlayerNum == 1) {
      dropSystemPlayerBomb(yourGrid);
    }

    // create game entity
    Game game = new Game(yourGrid, opponentGrid);
    PlayerEntity yourPlayerEntity = playerService.getAnonymousPlayer();
    PlayerEntity opponentPlayerEntity = playerService.getSystemPlayer();
    GameEntity gameEntity = ModelEntityMapper.toGame(game, yourPlayerEntity, opponentPlayerEntity);
    gameEntity.setTurnPlayerNum(yourPlayerNum);

    // get game player entities
    GamePlayerEntity[] gamePlayerEntities =
        gameEntity.getGamePlayers().toArray(new GamePlayerEntity[0]);
    GamePlayerEntity yourGamePlayerEntity = gamePlayerEntities[0];
    GamePlayerEntity opponentGamePlayerEntity = gamePlayerEntities[1];

    // save game entity
    gameEntity = gameRepository.save(gameEntity);

    // return game
    return EntityModelMapper.toGame(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity);
  }

  public Game dropBomb(String gameId, String gamePlayerId, Coordinate coordinate) {
    // get game entity
    GameEntity gameEntity = getGameEntity(gameId);

    // get game player entities
    GamePlayerEntity[] gamePlayerEntities =
        GameUtil.getGamePlayerEntities(gameEntity, gamePlayerId);
    GamePlayerEntity yourGamePlayerEntity = gamePlayerEntities[0];
    GamePlayerEntity opponentGamePlayerEntity = gamePlayerEntities[1];

    // validate your turn to drop bomb
    if (gameEntity.getTurnPlayerNum() == null) {
      throw new GameOverBombDropException();
    }
    if (!gameEntity.getTurnPlayerNum().equals(yourGamePlayerEntity.getPlayerNum())) {
      throw new NotTurnBombDropException();
    }

    // drop bomb
    Game game =
        EntityModelMapper.toGame(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity);
    BombedCoordinate bombedCoordinate = dropBomb(game.getOpponentGrid(), coordinate);
    yourGamePlayerEntity
        .getBombs()
        .add(ModelEntityMapper.toBomb(bombedCoordinate, yourGamePlayerEntity));

    // set turn/winning player num
    boolean gameOver = GameUtil.isAllShipsSunk(game.getOpponentGrid());
    if (gameOver) {
      gameEntity.setTurnPlayerNum(null);
      gameEntity.setWinningPlayerNum(yourGamePlayerEntity.getPlayerNum());
    } else {
      gameEntity.setTurnPlayerNum(opponentGamePlayerEntity.getPlayerNum());
    }

    // save game entity
    gameEntity = gameRepository.save(gameEntity);

    // drop system player bomb if game not over
    if (!gameOver) {
      // drop system player bomb
      bombedCoordinate = dropSystemPlayerBomb(game.getYourGrid());
      opponentGamePlayerEntity
          .getBombs()
          .add(ModelEntityMapper.toBomb(bombedCoordinate, opponentGamePlayerEntity));

      // set turn/winning player num
      gameOver = GameUtil.isAllShipsSunk(game.getYourGrid());
      if (gameOver) {
        gameEntity.setTurnPlayerNum(null);
        gameEntity.setWinningPlayerNum(opponentGamePlayerEntity.getPlayerNum());
      } else {
        gameEntity.setTurnPlayerNum(yourGamePlayerEntity.getPlayerNum());
      }

      // save game entity
      gameEntity = gameRepository.save(gameEntity);
    }

    // return game
    return EntityModelMapper.toGame(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity);
  }

  public Game endGame(String gameId, String gamePlayerId) {
    // get game entity
    GameEntity gameEntity = getGameEntity(gameId);

    // get game player entities
    GamePlayerEntity[] gamePlayerEntities =
        GameUtil.getGamePlayerEntities(gameEntity, gamePlayerId);
    GamePlayerEntity yourGamePlayerEntity = gamePlayerEntities[0];
    GamePlayerEntity opponentGamePlayerEntity = gamePlayerEntities[1];

    // set null turn if not null and save
    if (gameEntity.getTurnPlayerNum() != null) {
      gameEntity.setTurnPlayerNum(null);
      gameRepository.save(gameEntity);
    }

    // return game
    return EntityModelMapper.toGame(gameEntity, yourGamePlayerEntity, opponentGamePlayerEntity);
  }

  BombedCoordinate dropBomb(Grid grid, Coordinate coordinate) {
    // validate coordinate
    if (grid.getBombedCoordinates().contains(coordinate)) {
      throw new CoordinateAlreadyBombedException(coordinate);
    }

    List<Coordinate> availableCoordinates = GameUtil.getAvailableCoordinates(grid);
    if (!availableCoordinates.contains(coordinate)) {
      throw new InvalidBombCoordinateException(coordinate);
    }

    // validate game over
    if (GameUtil.isAllShipsSunk(grid)) {
      throw new GameOverBombDropException();
    }

    // drop bomb
    BombedCoordinate bombedCoordinate = new BombedCoordinate(coordinate);
    grid.getBombedCoordinates().add(bombedCoordinate);

    // determine if hit/sunken ship
    for (Ship ship : grid.getShips()) {
      for (Coordinate shipCoordinate : ship.getCoordinates()) {
        if (shipCoordinate.equals(coordinate)) {
          bombedCoordinate.setHit(true);
          if (GameUtil.isSunkenShip(ship, grid)) {
            bombedCoordinate.setSunkenShip(ship);
          }
          break;
        }
      }
      if (bombedCoordinate.isHit()) {
        break;
      }
    }

    return bombedCoordinate;
  }

  BombedCoordinate dropSystemPlayerBomb(Grid grid) {
    // get hit coordinates not associated with a sunken ship in order
    TreeSet<Coordinate> hitCoordinateSet = new TreeSet<>();
    grid.getBombedCoordinates()
        .forEach(
            bombedCoordinate -> {
              if (bombedCoordinate.isHit()) {
                hitCoordinateSet.add(bombedCoordinate);
                if (bombedCoordinate.getSunkenShip() != null) {
                  hitCoordinateSet.removeAll(bombedCoordinate.getSunkenShip().getCoordinates());
                }
              }
            });

    if (!hitCoordinateSet.isEmpty()) {
      HashSet<Coordinate> availableCoordinates =
          new HashSet<>(GameUtil.getAvailableCoordinates(grid));

      // get adjacent hit coordinates
      List<Pair<Coordinate, Coordinate>> adjacentHitCoordinates = new ArrayList<>();
      Coordinate[] hitCoordinates = hitCoordinateSet.toArray(new Coordinate[0]);
      for (int i = 0; i < hitCoordinates.length - 1; i++) {
        Coordinate coordinate1 = hitCoordinates[i];
        for (int j = i + 1; j < hitCoordinates.length; j++) {
          Coordinate coordinate2 = hitCoordinates[j];
          if (GridUtil.isAdjacentCoordinates(coordinate1, coordinate2)) {
            adjacentHitCoordinates.add(Pair.of(coordinate1, coordinate2));
          }
        }
      }

      // bomb adjacent hit path coordinate if available
      for (Pair<Coordinate, Coordinate> pair : adjacentHitCoordinates) {
        Coordinate coordinate1 = pair.getFirst();
        Coordinate coordinate2 = pair.getSecond();

        if (coordinate1.getX() == coordinate2.getX()) {
          // direction vertical
          Coordinate nextCoordinate = new Coordinate(coordinate1.getX(), coordinate2.getY() + 1);
          while (hitCoordinateSet.contains(nextCoordinate)) {
            nextCoordinate = new Coordinate(coordinate1.getX(), nextCoordinate.getY() + 1);
          }
          if (availableCoordinates.contains(nextCoordinate)) {
            return dropBomb(grid, nextCoordinate);
          }

          Coordinate prevCoordinate = new Coordinate(coordinate1.getX(), coordinate1.getY() - 1);
          if (availableCoordinates.contains(prevCoordinate)) {
            return dropBomb(grid, prevCoordinate);
          }
        } else {
          // direction horizontal
          Coordinate nextCoordinate = new Coordinate(coordinate2.getX() + 1, coordinate1.getY());
          while (hitCoordinateSet.contains(nextCoordinate)) {
            nextCoordinate = new Coordinate(nextCoordinate.getX() + 1, coordinate1.getY());
          }
          if (availableCoordinates.contains(nextCoordinate)) {
            return dropBomb(grid, nextCoordinate);
          }

          Coordinate prevCoordinate = new Coordinate(coordinate1.getX() - 1, coordinate1.getY());
          if (availableCoordinates.contains(prevCoordinate)) {
            return dropBomb(grid, prevCoordinate);
          }
        }
      }

      // bomb random adjacent hit coordinate if available
      for (Coordinate coordinate : hitCoordinateSet) {
        Coordinate[] adjacentCoordinates = {
          new Coordinate(coordinate.getX() + 1, coordinate.getY()),
          new Coordinate(coordinate.getX() - 1, coordinate.getY()),
          new Coordinate(coordinate.getX(), coordinate.getY() + 1),
          new Coordinate(coordinate.getX(), coordinate.getY() - 1),
        };
        List<Coordinate> availableAdjacentCoordinates =
            Arrays.asList(adjacentCoordinates)
                .stream()
                .filter(adjacentCoordinate -> availableCoordinates.contains(adjacentCoordinate))
                .collect(Collectors.toList());
        if (!availableAdjacentCoordinates.isEmpty()) {
          Coordinate randomAdjacentCoordinate =
              availableAdjacentCoordinates.get(random.nextInt(availableAdjacentCoordinates.size()));
          return dropBomb(grid, randomAdjacentCoordinate);
        }
      }
    }

    // bomb random available coordinate
    Coordinate coordinate = GameUtil.getRandomAvailableCoordinate(grid, random);
    return dropBomb(grid, coordinate);
  }

  GameEntity getGameEntity(String gameId) {
    Optional<GameEntity> gameEntity = gameRepository.findById(gameId);
    if (!gameEntity.isPresent()) {
      throw new GameNotFoundException(gameId);
    }
    return gameEntity.get();
  }
}
