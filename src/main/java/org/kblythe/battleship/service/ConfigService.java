package org.kblythe.battleship.service;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Random;
import org.kblythe.battleship.exception.GridConfigNotFoundException;
import org.kblythe.battleship.model.AppConfig;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.util.GridUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ConfigService {
  private LinkedHashMap<String, GridConfig> gridConfigMap = new LinkedHashMap<>();
  private Random random = new Random();

  public ConfigService() {
    this(new ClassPathResource("battleship-config.json"));
  }

  public ConfigService(Resource resource) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      AppConfig appConfig = mapper.readValue(resource.getInputStream(), AppConfig.class);
      appConfig
          .getGridConfigs()
          .forEach(gridConfig -> gridConfigMap.put(gridConfig.getId(), gridConfig));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void setRandom(Random random) {
    this.random = random;
  }

  public Collection<GridConfig> getGridConfigs() {
    return gridConfigMap.values();
  }

  public GridConfig getGridConfig(String id) {
    GridConfig gridConfig = gridConfigMap.get(id);
    if (gridConfig == null) {
      throw new GridConfigNotFoundException(id);
    }
    return gridConfig;
  }

  public Grid createGrid(String gridConfigId) {
    Grid grid = null;
    GridConfig gridConfig = getGridConfig(gridConfigId);
    if (gridConfig != null) {
      grid = new Grid(gridConfig);
      GridUtil.randomlyPlaceShips(grid, random);
    }
    return grid;
  }
}
