package org.kblythe.battleship.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import org.junit.jupiter.api.Test;

public class GridSizeTest {
  @Test
  public void testCompareToEquals() {
    // given
    GridSize gridSize = new GridSize(2, 2);

    // then
    assertThat(gridSize.compareTo(new GridSize(2, 2)), equalTo(0));
  }

  @Test
  public void testCompareToLessThan() {
    // given
    GridSize gridSize = new GridSize(2, 2);

    // then
    assertThat(gridSize, lessThan(new GridSize(3, 3)));
    assertThat(gridSize, lessThan(new GridSize(2, 3)));
    assertThat(gridSize, lessThan(new GridSize(3, 2)));
  }

  @Test
  public void testCompareToGreaterThan() {
    // given
    GridSize gridSize = new GridSize(2, 2);

    // then
    assertThat(gridSize, greaterThan(new GridSize(1, 1)));
    assertThat(gridSize, greaterThan(new GridSize(1, 2)));
    assertThat(gridSize, greaterThan(new GridSize(2, 1)));
  }
}
