package org.kblythe.battleship.model;

import org.junit.jupiter.api.Test;
import org.kblythe.battleship.testHelper.PojoTestHelper;

public class ModelPojoTest {
  static final String POJO_PACKAGE = "org.kblythe.battleship.model";
  static final int EXPECTED_CLASS_COUNT = 8;

  @Test
  public void testPojos() {
    PojoTestHelper.testPojos(POJO_PACKAGE, EXPECTED_CLASS_COUNT);
  }
}
