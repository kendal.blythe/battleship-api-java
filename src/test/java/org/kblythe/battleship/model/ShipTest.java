package org.kblythe.battleship.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import org.junit.jupiter.api.Test;

public class ShipTest {
  @Test
  public void testCompareToEquals() {
    // given
    Ship ship = new Ship("a", 1);

    // then
    assertThat(ship.compareTo(new Ship("a", 1)), equalTo(0));
  }

  @Test
  public void testCompareToLessThan() {
    // given
    Ship ship = new Ship("b", 2);

    // then
    assertThat(ship, lessThan(new Ship("b", 1)));
    assertThat(ship, lessThan(new Ship("c", 2)));
  }

  @Test
  public void testCompareToGreaterThan() {
    // given
    Ship ship = new Ship("b", 2);

    // then
    assertThat(ship, greaterThan(new Ship("b", 3)));
    assertThat(ship, greaterThan(new Ship("a", 2)));
  }
}
