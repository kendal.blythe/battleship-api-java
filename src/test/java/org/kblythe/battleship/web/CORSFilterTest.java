package org.kblythe.battleship.web;

import static org.mockito.Mockito.mock;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

public class CORSFilterTest {
  MockHttpServletRequest request;
  HttpServletResponse response;
  FilterChain filterChain;
  CORSFilter filter;

  @BeforeEach
  public void beforeEach() {
    // given
    request = new MockHttpServletRequest();
    response = mock(HttpServletResponse.class);
    filterChain = mock(FilterChain.class);
    filter = new CORSFilter();
  }

  @Test
  public void testFilter() throws Exception {
    // when
    filter.doFilter(request, response, filterChain);
  }

  @Test
  public void testFilterWithOriginHeader() throws Exception {
    // given
    request.addHeader("Origin", "localhost");

    // when
    filter.doFilter(request, response, filterChain);
  }

  @Test
  public void testFilterWithOptionsMethod() throws Exception {
    // given
    request.setMethod("OPTIONS");

    // when
    filter.doFilter(request, response, filterChain);
  }
}
