package org.kblythe.battleship.util;

import org.junit.jupiter.api.Test;
import org.kblythe.battleship.testHelper.PojoTestHelper;

public class UtilPojoTest {
  static final String POJO_PACKAGE = "org.kblythe.battleship.util";
  static final int EXPECTED_CLASS_COUNT = 4;

  @Test
  public void testPojos() {
    PojoTestHelper.testPojos(POJO_PACKAGE, EXPECTED_CLASS_COUNT);
  }
}
