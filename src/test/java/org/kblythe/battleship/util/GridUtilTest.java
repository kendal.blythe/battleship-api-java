package org.kblythe.battleship.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridSize;
import org.kblythe.battleship.model.Ship;
import org.kblythe.battleship.testHelper.AssertTestHelper;

public class GridUtilTest {
  Random random;

  @BeforeEach
  void beforeEach() {
    random = new Random(1);
  }

  @Test
  public void testRandomlyPlaceShips() {
    for (int i = 0; i < 20; i++) { // test many times to hit all code paths for code coverage
      // given
      Grid grid = new Grid();
      grid.setSize(new GridSize(10, 10));

      // when
      for (int length = 1; length <= 5; length++) {
        Ship ship = new Ship();
        ship.setName(String.format("ship%d", length));
        ship.setLength(length);
        grid.getShips().add(ship);
      }
      GridUtil.randomlyPlaceShips(grid, random);
    }
  }

  @Test
  public void testAdjacentCoordinates() {
    // given
    Coordinate coordinate = new Coordinate(2, 2);

    // when/then
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(2, 1)), equalTo(true));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(1, 2)), equalTo(true));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(3, 2)), equalTo(true));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(2, 3)), equalTo(true));
  }

  @Test
  public void testNonAdjacentCoordinates() {
    // given
    Coordinate coordinate = new Coordinate(2, 2);

    // when/then
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(1, 1)), equalTo(false));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(3, 1)), equalTo(false));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(2, 2)), equalTo(false));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(1, 3)), equalTo(false));
    assertThat(GridUtil.isAdjacentCoordinates(coordinate, new Coordinate(3, 3)), equalTo(false));
  }

  @Test
  public void testShipAdjacentToOccupiedCoordinates() {
    // given
    Coordinate shipCoordinate = new Coordinate(2, 2);

    // when/then
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(2, 1), true);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(1, 2), true);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(3, 2), true);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(2, 3), true);
  }

  @Test
  public void testShipNotAdjacentToOccupiedCoordinates() {
    // given
    Coordinate shipCoordinate = new Coordinate(2, 2);

    // when/then
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(1, 1), false);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(3, 1), false);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(2, 2), false);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(1, 3), false);
    AssertTestHelper.assertIsShipAdjacentToOccupiedCoordinates(
        shipCoordinate, new Coordinate(3, 3), false);
  }
}
