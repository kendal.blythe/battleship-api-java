package org.kblythe.battleship.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import java.util.Collection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.exception.GridConfigNotFoundException;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.SpringComponentTestHelper;
import org.kblythe.battleship.util.GridUtil;
import org.springframework.core.io.ClassPathResource;
import com.fasterxml.jackson.core.JsonParseException;

public class ConfigServiceTest {
  static final String INVALID_GRID_CONFIG_ID = "abc123";

  ConfigService configService = new ConfigService();

  @BeforeEach
  void beforeEach() {
    SpringComponentTestHelper.init(configService);
  }

  @Test
  void testGetGridConfigs() {
    // when
    Collection<GridConfig> gridConfigs = configService.getGridConfigs();

    // then
    AssertTestHelper.assertGridConfigs(gridConfigs);
  }

  @Test
  void testGetGridConfig() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      GridConfig gridConfig = configService.getGridConfig(gridConfigId);

      // then
      AssertTestHelper.assertGridConfig(gridConfig);
    }
  }

  @Test
  void testGetGridConfigNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigNotFoundException.class,
            () -> {
              configService.getGridConfig(INVALID_GRID_CONFIG_ID);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID)));
  }

  @Test
  void testCreateGrid() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      Grid grid = configService.createGrid(gridConfigId);

      // then
      AssertTestHelper.assertGrid(grid);
      GridUtil.printGrid(grid, System.out);
    }
  }

  @Test
  void testCreateGridNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigNotFoundException.class,
            () -> {
              configService.createGrid(INVALID_GRID_CONFIG_ID);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID)));
  }

  @Test
  void testLoadGridConfigsException() {
    // when/then
    RuntimeException e =
        Assertions.assertThrows(
            RuntimeException.class,
            () -> {
              new ConfigService(new ClassPathResource("/application.yaml"));
            });
    assertThat(e.getCause().getClass(), equalTo(JsonParseException.class));
  }
}
