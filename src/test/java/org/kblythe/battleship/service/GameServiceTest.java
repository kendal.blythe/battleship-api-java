package org.kblythe.battleship.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import java.util.Iterator;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.exception.CoordinateAlreadyBombedException;
import org.kblythe.battleship.exception.GameNotFoundException;
import org.kblythe.battleship.exception.GameOverBombDropException;
import org.kblythe.battleship.exception.GridConfigMismatchException;
import org.kblythe.battleship.exception.InvalidBombCoordinateException;
import org.kblythe.battleship.exception.InvalidShipCoordinateException;
import org.kblythe.battleship.exception.NonadjacentShipCoordinatesException;
import org.kblythe.battleship.exception.NotTurnBombDropException;
import org.kblythe.battleship.exception.OverlappingShipCoordinateException;
import org.kblythe.battleship.exception.PlayerNotFoundException;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridSize;
import org.kblythe.battleship.model.Ship;
import org.kblythe.battleship.repository.GameRepository;
import org.kblythe.battleship.repository.PlayerRepository;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.SpringComponentTestHelper;
import org.kblythe.battleship.util.GameUtil;
import org.mockito.Mockito;

public class GameServiceTest {
  PlayerRepository playerRepository = Mockito.mock(PlayerRepository.class);
  GameRepository gameRepository = Mockito.mock(GameRepository.class);
  ConfigService configService = new ConfigService();
  PlayerService playerService = new PlayerService(playerRepository);
  GameService gameService = new GameService(configService, playerService, gameRepository);
  Random random;

  @BeforeEach
  void beforeEach() {
    SpringComponentTestHelper.init(configService);
    SpringComponentTestHelper.init(gameService);
    SpringComponentTestHelper.init(playerRepository);
    SpringComponentTestHelper.init(gameRepository);
    random = new Random(1);
  }

  @Test
  void testStartGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // given
      Grid grid = configService.createGrid(gridConfigId);

      // when
      Game game = gameService.startGame(grid);

      // then
      AssertTestHelper.assertGame(game);
    }
  }

  @Test
  void testPlayGame() {
    for (int i = 0; i < 20; i++) { // test many times for code coverage
      for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
        // when
        Grid grid = configService.createGrid(gridConfigId);
        Game game = gameService.startGame(grid);
        while (game.getWinningPlayerNum() == null) {
          Coordinate coordinate =
              GameUtil.getRandomAvailableCoordinate(game.getOpponentGrid(), random);
          game = gameService.dropBomb(game.getId(), game.getYourGrid().getPlayerId(), coordinate);
        }

        // then
        assertThat(game.getWinningPlayerNum(), anyOf(equalTo(1), equalTo(2)));
      }
    }
  }

  @Test
  void testGameNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GameNotFoundException.class,
            () -> {
              gameService.getGame("-", "-");
            });
    assertThat(throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.GAME_NOT_FOUND, "-")));
  }

  @Test
  void testPlayerNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            PlayerNotFoundException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              Game game = gameService.startGame(grid);
              gameService.getGame(game.getId(), "-");
            });
    assertThat(
        throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.PLAYER_NOT_FOUND, "-")));
  }

  @Test
  void testNotYourTurnBombDrop() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            NotTurnBombDropException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              Game game = gameService.startGame(grid);
              Coordinate coordinate =
                  GameUtil.getRandomAvailableCoordinate(game.getOpponentGrid(), new Random(1));
              game =
                  gameService.dropBomb(
                      game.getId(), game.getOpponentGrid().getPlayerId(), coordinate);
            });
    assertThat(throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.NOT_TURN_BOMB_DROP)));
  }

  @Test
  void testGridConfigMismatchSize() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigMismatchException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              grid.setSize(new GridSize(1, 1));
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(
            Errors.getMessage(
                ErrorKeys.GRID_CONFIG_MISMATCH, ConfigTestHelper.GRID_CONFIG_6.getName())));
  }

  @Test
  void testGridConfigMismatchShipCount() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigMismatchException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              grid.getShips().add(new Ship());
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(
            Errors.getMessage(
                ErrorKeys.GRID_CONFIG_MISMATCH, ConfigTestHelper.GRID_CONFIG_6.getName())));
  }

  @Test
  void testGridConfigMismatchShips() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigMismatchException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              grid.getShips().iterator().next().setLength(6);
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(
            Errors.getMessage(
                ErrorKeys.GRID_CONFIG_MISMATCH, ConfigTestHelper.GRID_CONFIG_6.getName())));
  }

  @Test
  void testGridConfigMismatchShipCoordinates() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigMismatchException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              grid.getShips().iterator().next().getCoordinates().add(new Coordinate(1, 1));
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(
            Errors.getMessage(
                ErrorKeys.GRID_CONFIG_MISMATCH, ConfigTestHelper.GRID_CONFIG_6.getName())));
  }

  @Test
  void testInvalidShipCoordinate() {
    // given
    Coordinate invalidCoordinate = new Coordinate(0, 0);

    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            InvalidShipCoordinateException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              Coordinate coordinate =
                  grid.getShips().iterator().next().getCoordinates().iterator().next();
              coordinate.setX(invalidCoordinate.getX());
              coordinate.setY(invalidCoordinate.getY());
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.INVALID_SHIP_COORDINATE, invalidCoordinate)));
  }

  @Test
  void testOverlappingShipCoordinate() {
    // given
    Coordinate validCoordinate = new Coordinate(1, 1);

    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            OverlappingShipCoordinateException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              Ship ship = grid.getShips().iterator().next();
              ship.getCoordinates()
                  .forEach(
                      coordinate -> {
                        coordinate.setX(validCoordinate.getX());
                        coordinate.setY(validCoordinate.getY());
                      });
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.OVERLAPPING_SHIP_COORDINATE, validCoordinate)));
  }

  @Test
  void testNonadjacentShipCoordinates() {
    // given
    Coordinate validCoordinate1 = new Coordinate(1, 1);
    Coordinate validCoordinate2 = new Coordinate(2, 2);

    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            NonadjacentShipCoordinatesException.class,
            () -> {
              Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
              Ship ship = grid.getShips().iterator().next();
              Iterator<Coordinate> iter = ship.getCoordinates().iterator();
              Coordinate coordinate1 = iter.next();
              Coordinate coordinate2 = iter.next();
              coordinate1.setX(validCoordinate1.getX());
              coordinate1.setY(validCoordinate1.getY());
              coordinate2.setX(validCoordinate2.getX());
              coordinate2.setY(validCoordinate2.getY());
              gameService.startGame(grid);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(
            Errors.getMessage(
                ErrorKeys.NONADJACENT_SHIP_COORDINATES, validCoordinate1, validCoordinate2)));
  }

  @Test
  void testCoordinateAlreadyBombed() {
    // given
    Coordinate validCoordinate = new Coordinate(1, 1);

    // when/then
    Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
    Game game = gameService.startGame(grid);
    gameService.dropBomb(game.getOpponentGrid(), validCoordinate);
    Throwable throwable =
        Assertions.assertThrows(
            CoordinateAlreadyBombedException.class,
            () -> {
              gameService.dropBomb(game.getOpponentGrid(), validCoordinate);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.COORDINATE_ALREADY_BOMBED, validCoordinate)));
  }

  @Test
  void testInvalidBombCoordinate() {
    // given
    Coordinate invalidCoordinate = new Coordinate(0, 0);

    // when/then
    Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
    Game game = gameService.startGame(grid);
    Throwable throwable =
        Assertions.assertThrows(
            InvalidBombCoordinateException.class,
            () -> {
              gameService.dropBomb(game.getOpponentGrid(), invalidCoordinate);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.INVALID_BOMB_COORDINATE, invalidCoordinate)));
  }

  @Test
  void testGameOverBombDrop1() {
    // given
    Ship ship = new Ship("Test", 1);
    ship.getCoordinates().add(new Coordinate(1, 1));

    GridSize size = new GridSize(3, 3);

    Grid grid = new Grid();
    grid.setSize(size);
    grid.getShips().add(ship);

    gameService.dropBomb(grid, new Coordinate(1, 1));

    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GameOverBombDropException.class,
            () -> {
              gameService.dropBomb(grid, new Coordinate(2, 2));
            });
    assertThat(throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.GAME_OVER_BOMB_DROP)));
  }

  @Test
  void testGameOverBombDrop2() {
    // given
    Ship ship = new Ship("Test", 1);
    ship.getCoordinates().add(new Coordinate(3, 3));

    GridSize size = new GridSize(3, 3);

    Grid grid = new Grid();
    grid.setSize(size);
    grid.getShips().add(ship);

    for (int x = 1; x <= size.getX(); x++) {
      for (int y = 1; y <= size.getY(); y++) {
        gameService.dropBomb(grid, new Coordinate(x, y));
      }
    }

    // when/the
    Throwable throwable =
        Assertions.assertThrows(
            GameOverBombDropException.class,
            () -> {
              GameUtil.getRandomAvailableCoordinate(grid, new Random(1));
            });
    assertThat(throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.GAME_OVER_BOMB_DROP)));
  }

  @Test
  void testGameOverBombDrop3() {
    // given
    Grid grid = configService.createGrid(ConfigTestHelper.GRID_CONFIG_6.getId());
    Game game = gameService.startGame(grid);
    gameService.endGame(game.getId(), game.getYourGrid().getPlayerId());
    Coordinate coordinate =
        GameUtil.getRandomAvailableCoordinate(game.getOpponentGrid(), new Random(1));

    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GameOverBombDropException.class,
            () -> {
              gameService.dropBomb(game.getId(), game.getOpponentGrid().getPlayerId(), coordinate);
            });
    assertThat(throwable.getMessage(), endsWith(Errors.getMessage(ErrorKeys.GAME_OVER_BOMB_DROP)));
  }
}
