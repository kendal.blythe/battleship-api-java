package org.kblythe.battleship.l10n;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import java.util.MissingResourceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ErrorsTest {
  static final String EXPECTED_GAME_OVER_BOMB_DROP_MESSAGE =
      "Not player turn to drop bomb - game over.";
  static final String EXPECTED_GRID_CONFIG_NOT_FOUND_MESSAGE_FORMAT =
      "Grid configuration not found for id '%s'.";

  @Test
  public void testConstructor() {
    new Errors();
  }

  @Test
  void testGetMessage() {
    // when
    String message = Errors.getMessage(ErrorKeys.GAME_OVER_BOMB_DROP);

    // then
    assertThat(message, equalTo(EXPECTED_GAME_OVER_BOMB_DROP_MESSAGE));
  }

  @Test
  void testGetLocalizedMessage() {
    // when
    String localizedMessage = Errors.getLocalizedMessage(ErrorKeys.GAME_OVER_BOMB_DROP);

    // then
    assertThat(localizedMessage, equalTo(EXPECTED_GAME_OVER_BOMB_DROP_MESSAGE));
  }

  @Test
  void testGetMessageWithParameter() {
    // given
    String id = "-";
    String expectedMessage = String.format(EXPECTED_GRID_CONFIG_NOT_FOUND_MESSAGE_FORMAT, id);

    // when
    String message = Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, id);

    // then
    assertThat(message, equalTo(expectedMessage));
  }

  @Test
  void testGetLocalizedMessageWithParameter() {
    // given
    String id = "-";
    String expectedMessage = String.format(EXPECTED_GRID_CONFIG_NOT_FOUND_MESSAGE_FORMAT, id);

    // when
    String localizedMessage = Errors.getLocalizedMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, id);

    // then
    assertThat(localizedMessage, equalTo(expectedMessage));
  }

  @Test
  void testInvalidGetMessageKey() {
    Assertions.assertThrows(
        MissingResourceException.class,
        () -> {
          Errors.getMessage("-");
        });
  }

  @Test
  void testInvalidGetLocalizedMessageKey() {
    Assertions.assertThrows(
        MissingResourceException.class,
        () -> {
          Errors.getLocalizedMessage("-");
        });
  }

  @Test
  void testInvalidGetMessageKeyWithParameter() {
    Assertions.assertThrows(
        MissingResourceException.class,
        () -> {
          Errors.getMessage("-", "-");
        });
  }

  @Test
  void testInvalidGetLocalizedMessageKeyWithParameter() {
    Assertions.assertThrows(
        MissingResourceException.class,
        () -> {
          Errors.getLocalizedMessage("-", "-");
        });
  }
}
