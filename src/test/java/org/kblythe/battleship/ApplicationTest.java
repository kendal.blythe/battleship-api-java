package org.kblythe.battleship;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.SpringApplication;

@RunWith(PowerMockRunner.class)
public class ApplicationTest {
  @Test
  public void testConstructor() {
    new Application();
  }

  @Test
  @PrepareForTest(SpringApplication.class)
  public void testMain() {
    mockStatic(SpringApplication.class);
    Application.main();
  }
}
