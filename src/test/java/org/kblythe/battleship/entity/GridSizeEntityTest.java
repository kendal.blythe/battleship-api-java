package org.kblythe.battleship.entity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import org.junit.jupiter.api.Test;

public class GridSizeEntityTest {
  @Test
  public void testEquals() {
    // given
    GridSizeEntity gridSize = new GridSizeEntity(1, 1);

    // then
    assertThat(gridSize, equalTo(new GridSizeEntity(1, 1)));
  }

  @Test
  public void testNotEquals() {
    // given
    GridSizeEntity gridSize = new GridSizeEntity(1, 1);

    // then
    assertThat(gridSize, not(equalTo(new GridSizeEntity(2, 2))));
  }

  @Test
  public void testNotEqualsNull() {
    // given
    GridSizeEntity gridSize = new GridSizeEntity(1, 1);

    // then
    assertThat(gridSize, not(equalTo(null)));
  }
}
