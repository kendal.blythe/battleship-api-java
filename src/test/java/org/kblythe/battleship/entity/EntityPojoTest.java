package org.kblythe.battleship.entity;

import org.junit.jupiter.api.Test;
import org.kblythe.battleship.testHelper.PojoTestHelper;

public class EntityPojoTest {
  static final String POJO_PACKAGE = "org.kblythe.battleship.entity";
  static final int EXPECTED_CLASS_COUNT = 8;

  @Test
  public void testPojos() {
    PojoTestHelper.testPojos(POJO_PACKAGE, EXPECTED_CLASS_COUNT);
  }
}
