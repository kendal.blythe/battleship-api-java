package org.kblythe.battleship.entity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import org.junit.jupiter.api.Test;

public class CoordinateEntityTest {
  @Test
  public void testEquals() {
    // given
    CoordinateEntity gridSize = new CoordinateEntity(1, 1);

    // then
    assertThat(gridSize, equalTo(new CoordinateEntity(1, 1)));
  }

  @Test
  public void testNotEquals() {
    // given
    CoordinateEntity gridSize = new CoordinateEntity(1, 1);

    // then
    assertThat(gridSize, not(equalTo(new CoordinateEntity(2, 2))));
  }

  @Test
  public void testNotEqualsNull() {
    // given
    CoordinateEntity gridSize = new CoordinateEntity(1, 1);

    // then
    assertThat(gridSize, not(equalTo(null)));
  }
}
