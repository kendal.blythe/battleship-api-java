package org.kblythe.battleship.graphql;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.exception.GameOverBombDropException;
import org.kblythe.battleship.testHelper.TestExceptionWhileDataFetching;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.execution.ExecutionPath;

public class ErrorHandlerTest {
  @Test
  void testProcessErrors() {
    // given
    GraphQLError error1 = new TestExceptionWhileDataFetching(new GameOverBombDropException());
    ExceptionWhileDataFetching error2 =
        new ExceptionWhileDataFetching(
            ExecutionPath.rootPath(), new GameOverBombDropException(), null);

    // when
    List<GraphQLError> errors = new ErrorHandler().processErrors(Arrays.asList(error1, error2));

    // then
    assertThat(errors, hasSize(2));
  }
}
