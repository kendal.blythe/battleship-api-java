package org.kblythe.battleship.graphql;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import java.util.Collection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.exception.GridConfigNotFoundException;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.repository.GameRepository;
import org.kblythe.battleship.repository.PlayerRepository;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.service.GameService;
import org.kblythe.battleship.service.PlayerService;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.SpringComponentTestHelper;
import org.mockito.Mockito;

public class ConfigResolverTest {
  static final String INVALID_GRID_CONFIG_ID = "abc123";

  PlayerRepository playerRepository = Mockito.mock(PlayerRepository.class);
  GameRepository gameRepository = Mockito.mock(GameRepository.class);
  ConfigService configService = new ConfigService();
  PlayerService playerService = new PlayerService(playerRepository);
  GameService gameService = new GameService(configService, playerService, gameRepository);
  QueryResolver queryResolver = new QueryResolver(configService, gameService);
  MutationResolver mutationResolver = new MutationResolver(configService, gameService);

  @BeforeEach
  void beforeEach() {
    SpringComponentTestHelper.init(configService);
    SpringComponentTestHelper.init(gameService);
    SpringComponentTestHelper.init(playerRepository);
    SpringComponentTestHelper.init(gameRepository);
  }

  @Test
  void testGetGridConfigs() {
    // when
    Collection<GridConfig> gridConfigs = queryResolver.getGridConfigs();

    // then
    AssertTestHelper.assertGridConfigs(gridConfigs);
  }

  @Test
  void testGetGridConfig() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      GridConfig gridConfig = queryResolver.getGridConfig(gridConfigId);

      // then
      AssertTestHelper.assertGridConfig(gridConfig);
    }
  }

  @Test
  void testGetGridConfigNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigNotFoundException.class,
            () -> {
              queryResolver.getGridConfig(INVALID_GRID_CONFIG_ID);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID)));
  }

  @Test
  void testCreateGrid() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      Grid grid = mutationResolver.createGrid(gridConfigId);

      // then
      AssertTestHelper.assertGrid(grid);
    }
  }

  @Test
  void testCreateGridNotFound() {
    // when/then
    Throwable throwable =
        Assertions.assertThrows(
            GridConfigNotFoundException.class,
            () -> {
              mutationResolver.createGrid(INVALID_GRID_CONFIG_ID);
            });
    assertThat(
        throwable.getMessage(),
        endsWith(Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID)));
  }
}
