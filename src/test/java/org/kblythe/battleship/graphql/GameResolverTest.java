package org.kblythe.battleship.graphql;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.repository.GameRepository;
import org.kblythe.battleship.repository.PlayerRepository;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.service.GameService;
import org.kblythe.battleship.service.PlayerService;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.GraphQLTestHelper;
import org.kblythe.battleship.testHelper.SpringComponentTestHelper;
import org.kblythe.battleship.util.GameUtil;
import org.mockito.Mockito;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionId;
import graphql.execution.ExecutionTypeInfo;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingFieldSelectionSet;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import graphql.servlet.GraphQLContext;

public class GameResolverTest {
  DataFetchingEnvironment env = GraphQLTestHelper.getMockDataFetchingEnvironment();
  PlayerRepository playerRepository = Mockito.mock(PlayerRepository.class);
  GameRepository gameRepository = Mockito.mock(GameRepository.class);
  ConfigService configService = new ConfigService();
  PlayerService playerService = new PlayerService(playerRepository);
  GameService gameService = new GameService(configService, playerService, gameRepository);
  QueryResolver queryResolver = new QueryResolver(configService, gameService);
  MutationResolver mutationResolver = new MutationResolver(configService, gameService);
  Random random;

  @BeforeEach
  void beforeEach() {
    env = GraphQLTestHelper.getMockDataFetchingEnvironment();
    SpringComponentTestHelper.init(configService);
    SpringComponentTestHelper.init(gameService);
    SpringComponentTestHelper.init(playerRepository);
    SpringComponentTestHelper.init(gameRepository);
    random = new Random(1);
  }

  @Test
  void testStartGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      Game game = mutationResolver.startGame(configService.createGrid(gridConfigId), env);

      // then
      AssertTestHelper.assertGame(game);
    }
  }

  @Test
  void testGetGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // given
      Game game = mutationResolver.startGame(configService.createGrid(gridConfigId), env);

      // when
      Game game2 = queryResolver.getGame(game.getId(), game.getYourGrid().getPlayerId());

      // then
      assertThat(game2.getId(), equalTo(game.getId()));
      assertThat(queryResolver.getCurrentGame(env), notNullValue());
    }
  }

  @Test
  void testEndGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // given
      Game game = mutationResolver.startGame(configService.createGrid(gridConfigId), env);

      // when
      boolean gameEnded =
          mutationResolver.endGame(game.getId(), game.getYourGrid().getPlayerId(), env);

      // then
      assertThat(gameEnded, equalTo(true));
      assertThat(queryResolver.getCurrentGame(env), nullValue());
    }
  }

  @Test
  void testPlayGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // when
      Grid grid = configService.createGrid(gridConfigId);
      Game game = gameService.startGame(grid);
      while (game.getWinningPlayerNum() == null) {
        Coordinate coordinate =
            GameUtil.getRandomAvailableCoordinate(game.getOpponentGrid(), random);
        game =
            mutationResolver.dropBomb(
                game.getId(), game.getYourGrid().getPlayerId(), coordinate, env);
      }

      // then
      assertThat(game.getWinningPlayerNum(), anyOf(equalTo(1), equalTo(2)));
    }
  }

  @Test
  void testHttpSession() {
    // given
    DataFetchingEnvironment testEnv =
        new DataFetchingEnvironment() {

          @Override
          public <T> T getSource() {
            return null;
          }

          @Override
          public DataFetchingFieldSelectionSet getSelectionSet() {
            return null;
          }

          @Override
          public <T> T getRoot() {
            return null;
          }

          @Override
          public GraphQLType getParentType() {
            return null;
          }

          @Override
          public GraphQLSchema getGraphQLSchema() {
            return null;
          }

          @Override
          public Map<String, FragmentDefinition> getFragmentsByName() {
            return null;
          }

          @Override
          public List<Field> getFields() {
            return null;
          }

          @Override
          public ExecutionTypeInfo getFieldTypeInfo() {
            return null;
          }

          @Override
          public GraphQLOutputType getFieldType() {
            return null;
          }

          @Override
          public GraphQLFieldDefinition getFieldDefinition() {
            return null;
          }

          @Override
          public Field getField() {
            return null;
          }

          @Override
          public ExecutionId getExecutionId() {
            return null;
          }

          @Override
          public ExecutionContext getExecutionContext() {
            return null;
          }

          @SuppressWarnings("unchecked")
          @Override
          public <T> T getContext() {
            return (T) new GraphQLContext();
          }

          @Override
          public Map<String, Object> getArguments() {
            return null;
          }

          @Override
          public <T> T getArgument(String name) {
            return null;
          }

          @Override
          public boolean containsArgument(String name) {
            return false;
          }
        };

    // when
    mutationResolver.getSessionAttribute("abc", testEnv);
    mutationResolver.setSessionAttribute("abc", "xyz", testEnv);
  }
}
