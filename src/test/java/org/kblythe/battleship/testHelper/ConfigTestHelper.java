package org.kblythe.battleship.testHelper;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.model.GridSize;
import org.kblythe.battleship.model.Ship;

public class ConfigTestHelper {
  public static final Map<String, GridConfig> EXPECTED_GRID_CONFIG_MAP;
  public static final GridConfig GRID_CONFIG_10;
  public static final GridConfig GRID_CONFIG_9;
  public static final GridConfig GRID_CONFIG_6;

  static {
    EXPECTED_GRID_CONFIG_MAP = new LinkedHashMap<>();

    GRID_CONFIG_10 = new GridConfig();
    GRID_CONFIG_10.setId("GRID_10_10_5");
    GRID_CONFIG_10.setName("10x10 (5 ships)");
    GRID_CONFIG_10.setSize(new GridSize(10, 10));
    GRID_CONFIG_10.setShips(
        new TreeSet<Ship>(
            Arrays.asList(
                new Ship("Carrier", 5),
                new Ship("Battleship", 4),
                new Ship("Cruiser", 3),
                new Ship("Submarine", 3),
                new Ship("Destroyer", 2))));
    EXPECTED_GRID_CONFIG_MAP.put(GRID_CONFIG_10.getId(), GRID_CONFIG_10);

    GRID_CONFIG_9 = new GridConfig();
    GRID_CONFIG_9.setId("GRID_9_9_4");
    GRID_CONFIG_9.setName("9x9 (4 ships)");
    GRID_CONFIG_9.setSize(new GridSize(9, 9));
    GRID_CONFIG_9.setShips(
        new TreeSet<Ship>(
            Arrays.asList(
                new Ship("Carrier", 5),
                new Ship("Battleship", 4),
                new Ship("Submarine", 3),
                new Ship("Patrol Boat", 2))));
    EXPECTED_GRID_CONFIG_MAP.put(GRID_CONFIG_9.getId(), GRID_CONFIG_9);

    GRID_CONFIG_6 = new GridConfig();
    GRID_CONFIG_6.setId("GRID_6_6_3");
    GRID_CONFIG_6.setName("6x6 (3 ships)");
    GRID_CONFIG_6.setSize(new GridSize(6, 6));
    GRID_CONFIG_6.setShips(
        new TreeSet<Ship>(
            Arrays.asList(
                new Ship("Battleship", 3), new Ship("Destroyer", 2), new Ship("Submarine", 1))));
    EXPECTED_GRID_CONFIG_MAP.put(GRID_CONFIG_6.getId(), GRID_CONFIG_6);
  }

  public static Collection<String> getGridConfigIds() {
    return EXPECTED_GRID_CONFIG_MAP.keySet();
  }

  public static Collection<GridConfig> getGridConfigs() {
    return EXPECTED_GRID_CONFIG_MAP.values();
  }
}
