package org.kblythe.battleship.testHelper;

import java.util.List;
import java.util.Map;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

public class TestExceptionWhileDataFetching implements GraphQLError {
  private static final long serialVersionUID = 1L;

  private String message;
  private List<Object> path;
  private Throwable exception;
  private List<SourceLocation> locations;
  private Map<String, Object> extensions;

  public TestExceptionWhileDataFetching() {}

  public TestExceptionWhileDataFetching(Throwable exception) {
    this.exception = exception;
    this.message = exception.getMessage();
  }

  @Override
  public ErrorType getErrorType() {
    return ErrorType.DataFetchingException;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public List<Object> getPath() {
    return path;
  }

  public void setPath(List<Object> path) {
    this.path = path;
  }

  public Throwable getException() {
    return exception;
  }

  public void setException(Throwable exception) {
    this.exception = exception;
  }

  @Override
  public List<SourceLocation> getLocations() {
    return locations;
  }

  public void setLocations(List<SourceLocation> locations) {
    this.locations = locations;
  }

  @Override
  public Map<String, Object> getExtensions() {
    return extensions;
  }

  public void setExtensions(Map<String, Object> extensions) {
    this.extensions = extensions;
  }
}
