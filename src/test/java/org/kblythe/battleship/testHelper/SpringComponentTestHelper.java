package org.kblythe.battleship.testHelper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import org.kblythe.battleship.entity.GameEntity;
import org.kblythe.battleship.entity.PlayerEntity;
import org.kblythe.battleship.repository.GameRepository;
import org.kblythe.battleship.repository.PlayerRepository;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.service.GameService;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SpringComponentTestHelper {
  private static final Map<String, GameEntity> GAME_MAP = new HashMap<>();

  public static void init(ConfigService configService) {
    // set random generator with constant seed
    configService.setRandom(new Random(1));
  }

  public static void init(GameService gameService) {
    // set random generator with constant seed
    gameService.setRandom(new Random(1));
  }

  public static void init(PlayerRepository playerRepository) {
    // add system player
    PlayerEntity systemPlayer = new PlayerEntity(PlayerEntity.ID_SYSTEM, "System");
    Mockito.when(playerRepository.findById(systemPlayer.getId()))
        .thenReturn(Optional.ofNullable(systemPlayer));

    // add anonymous player
    PlayerEntity anonymousPlayer = new PlayerEntity(PlayerEntity.ID_ANONYMOUS, "Anonymous");
    Mockito.when(playerRepository.findById(anonymousPlayer.getId()))
        .thenReturn(Optional.ofNullable(anonymousPlayer));

    // test AbstractIdentifiableEntity equals for code coverage
    assertThat(systemPlayer, equalTo(new PlayerEntity(PlayerEntity.ID_SYSTEM, "System")));
    assertThat(systemPlayer, not(equalTo(anonymousPlayer)));
    assertThat(systemPlayer, not(equalTo(null)));
  }

  public static void init(GameRepository gameRepository) {
    // mock save()
    Mockito.when(gameRepository.save(Mockito.any(GameEntity.class)))
        .thenAnswer(
            new Answer<GameEntity>() {
              @Override
              public GameEntity answer(InvocationOnMock invocation) {
                GameEntity game = (GameEntity) invocation.getArgument(0);
                Date date = new Date();
                if (game.getId() == null) {
                  game.prePersist();
                  game.setCreatedDate(date);
                }
                game.setLastModifiedDate(date);
                game.getGamePlayers()
                    .forEach(
                        gamePlayer -> {
                          if (gamePlayer.getId() == null) {
                            gamePlayer.prePersist();
                          }
                          gamePlayer
                              .getShips()
                              .forEach(
                                  ship -> {
                                    if (ship.getId() == null) {
                                      ship.prePersist();
                                    }
                                  });
                          gamePlayer
                              .getBombs()
                              .forEach(
                                  bomb -> {
                                    if (bomb.getId() == null) {
                                      bomb.prePersist();
                                    }
                                  });
                        });
                GAME_MAP.put(game.getId(), game);
                return game;
              }
            });

    // mock findById()
    Mockito.when(gameRepository.findById(Mockito.anyString()))
        .thenAnswer(
            new Answer<Optional<GameEntity>>() {
              @Override
              public Optional<GameEntity> answer(InvocationOnMock invocation) {
                String gameId = (String) invocation.getArgument(0);
                GameEntity game = GAME_MAP.get(gameId);
                return Optional.ofNullable(game);
              }
            });
  }
}
