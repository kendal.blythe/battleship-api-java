package org.kblythe.battleship.testHelper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.StreamUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.GraphQLError;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionContextBuilder;
import graphql.execution.ExecutionId;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingEnvironmentBuilder;
import graphql.servlet.GraphQLContext;

public class GraphQLTestHelper {
  private static final HttpHeaders headers;
  private static final ObjectMapper mapper;

  static {
    headers = new HttpHeaders();
    headers.setContentType(new MediaType("application", "graphql"));
    headers.put(HttpHeaders.COOKIE, new ArrayList<String>());

    mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public static DataFetchingEnvironment getMockDataFetchingEnvironment() {
    MockHttpSession session = new MockHttpSession();
    MockServletContext servletCtx = new MockServletContext();
    MockHttpServletRequest request =
        MockMvcRequestBuilders.get("/battleship/graphql").session(session).buildRequest(servletCtx);
    GraphQLContext graphqlCtx = new GraphQLContext(request);
    ExecutionContext executionCtx =
        ExecutionContextBuilder.newExecutionContextBuilder()
            .executionId(ExecutionId.generate())
            .context(graphqlCtx)
            .build();
    return DataFetchingEnvironmentBuilder.newDataFetchingEnvironment()
        .executionContext(executionCtx)
        .context(graphqlCtx)
        .build();
  }

  public static String getQueryString(Resource gqlResource, Object... args) {
    try {
      String queryString =
          StreamUtils.copyToString(gqlResource.getInputStream(), StandardCharsets.UTF_8);
      Object[] jsonArgs =
          Arrays.stream(args)
              .map(GraphQLTestHelper::objectToArg)
              .collect(Collectors.toList())
              .toArray();
      return String.format(queryString, jsonArgs);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static ResponseEntity<TestGraphQLResponseMap> query(
      TestRestTemplate restTemplate, String url, String... queryStrings) {
    // build query
    StringBuilder queryBuilder = new StringBuilder("query {\n");
    Arrays.stream(queryStrings)
        .forEach(queryString -> queryBuilder.append(queryString).append("\n"));
    queryBuilder.append("}");

    // post query
    HttpEntity<String> request = new HttpEntity<>(queryBuilder.toString(), headers);
    return restTemplate.postForEntity(url, request, TestGraphQLResponseMap.class);
  }

  public static String getMutationString(Resource gqlResource, Object... args) {
    try {
      String mutationString =
          StreamUtils.copyToString(gqlResource.getInputStream(), StandardCharsets.UTF_8);
      Object[] jsonArgs =
          Arrays.stream(args)
              .map(GraphQLTestHelper::objectToArg)
              .collect(Collectors.toList())
              .toArray();
      return String.format(mutationString, jsonArgs);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static ResponseEntity<TestGraphQLResponseMap> mutation(
      TestRestTemplate restTemplate, String url, String... mutationStrings) {
    // build mutation
    StringBuilder mutationBuilder = new StringBuilder("mutation {\n");
    Arrays.stream(mutationStrings)
        .forEach(mutationString -> mutationBuilder.append(mutationString).append("\n"));
    mutationBuilder.append("}");

    // post mutation
    HttpEntity<String> request = new HttpEntity<>(mutationBuilder.toString(), headers);
    return restTemplate.postForEntity(url, request, TestGraphQLResponseMap.class);
  }

  public static <T> T getData(
      ResponseEntity<TestGraphQLResponseMap> response, String operationName, Class<T> dataType) {
    return getData(response, operationName, mapper.getTypeFactory().constructType(dataType));
  }

  public static <T> T getData(
      ResponseEntity<TestGraphQLResponseMap> response,
      String operationName,
      TypeReference<T> dataType) {
    return getData(response, operationName, mapper.getTypeFactory().constructType(dataType));
  }

  @SuppressWarnings("rawtypes")
  public static <T> T getData(
      ResponseEntity<TestGraphQLResponseMap> response, String operationName, JavaType dataType) {
    TestGraphQLResponseMap body = response.getBody();
    if (body != null) {
      Map dataMap = (Map) body.get("data");
      if (dataMap != null) {
        Object dataObj = dataMap.get(operationName);
        if (dataObj != null) {
          String json = objectToJson(dataObj);
          return jsonToObject(json, dataType);
        }
      }
    }
    return null;
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public static List<GraphQLError> getErrors(ResponseEntity<TestGraphQLResponseMap> response) {
    List errorList = null;
    TestGraphQLResponseMap body = response.getBody();
    if (body != null) {
      String json = objectToJson(body.get("errors"));
      errorList = jsonToObject(json, new TypeReference<List<TestExceptionWhileDataFetching>>() {});
    }
    return (errorList == null || errorList.isEmpty()) ? null : errorList;
  }

  public static String objectToArg(Object obj) {
    String json = objectToJson(obj);
    Pattern pattern = Pattern.compile("\"([^\"]*)\":");
    Matcher matcher = pattern.matcher(json);
    HashSet<String> propSet = new HashSet<>();
    while (matcher.find()) {
      propSet.add(matcher.group(1));
    }
    for (String prop : propSet) {
      json = json.replace("\"" + prop + "\":", prop + ":");
    }
    return json;
  }

  public static String objectToJson(Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> T jsonToObject(String json, JavaType dataType) {
    try {
      return mapper.readValue(json, dataType);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> T jsonToObject(String json, TypeReference<T> valueTypeRef) {
    try {
      return mapper.readValue(json, valueTypeRef);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
