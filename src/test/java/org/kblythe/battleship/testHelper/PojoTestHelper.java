package org.kblythe.battleship.testHelper;

import java.util.List;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.affirm.Affirm;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

public class PojoTestHelper {
  public static void testPojos(String pojoPackage, int expectedClassCount) {
    // ensure expected class count in case classes are added/removed
    List<PojoClass> pojoClasses =
        PojoClassFactory.getPojoClasses(pojoPackage, TestPojoClassFilter.getInstance());
    Affirm.affirmEquals("Classes added / removed?", expectedClassCount, pojoClasses.size());

    // test default constructor, getters and setters
    Validator validator =
        ValidatorBuilder.create()
            .with(new GetterMustExistRule())
            .with(new SetterMustExistRule())
            .with(new SetterTester())
            .with(new GetterTester())
            .build();
    validator.validate(pojoPackage, TestPojoClassFilter.getInstance());
  }

  static class TestPojoClassFilter implements PojoClassFilter {
    private static PojoClassFilter instance = new TestPojoClassFilter();

    public static PojoClassFilter getInstance() {
      return instance;
    }

    @Override
    public boolean include(PojoClass pojoClass) {
      return pojoClass.getSourcePath().contains("/main/");
    }
  }
}
