package org.kblythe.battleship.testHelper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.stringContainsInOrder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.util.GridUtil;
import graphql.GraphQLError;

public class AssertTestHelper {
  public static void assertGridConfigs(Collection<GridConfig> gridConfigs) {
    assertThat(gridConfigs, notNullValue());
    assertThat(gridConfigs, hasSize(ConfigTestHelper.EXPECTED_GRID_CONFIG_MAP.size()));
    gridConfigs.stream().forEach(gridConfig -> assertGridConfig(gridConfig));
  }

  public static void assertGridConfig(GridConfig actual) {
    assertThat(actual, notNullValue());
    GridConfig expected = ConfigTestHelper.EXPECTED_GRID_CONFIG_MAP.get(actual.getId());
    assertThat(expected, notNullValue());
    assertThat(actual.getName(), equalTo(expected.getName()));
    assertThat(actual.getSize(), equalTo(expected.getSize()));
    assertThat(actual.getShips(), equalTo(expected.getShips()));
  }

  public static void assertGridConfigSummaries(Collection<GridConfig> gridConfigs) {
    assertThat(gridConfigs, notNullValue());
    assertThat(gridConfigs, hasSize(ConfigTestHelper.EXPECTED_GRID_CONFIG_MAP.size()));
    gridConfigs.stream().forEach(gridConfig -> assertGridConfigSummary(gridConfig));
  }

  public static void assertGridConfigSummary(GridConfig actual) {
    assertThat(actual, notNullValue());
    GridConfig expected = ConfigTestHelper.EXPECTED_GRID_CONFIG_MAP.get(actual.getId());
    assertThat(expected, notNullValue());
    assertThat(actual.getName(), equalTo(expected.getName()));
  }

  public static void assertGrid(Grid actual) {
    assertThat(actual, notNullValue());
    GridConfig expected = ConfigTestHelper.EXPECTED_GRID_CONFIG_MAP.get(actual.getGridConfigId());
    assertThat(actual.getSize(), equalTo(expected.getSize()));
    assertThat(actual.getShips(), hasSize(expected.getShips().size()));
    actual.getShips().forEach(ship -> assertThat(ship.getCoordinates(), hasSize(ship.getLength())));
    actual
        .getShips()
        .forEach(
            ship ->
                assertThat(expected.getShips(), hasItem(hasProperty("name", is(ship.getName())))));
  }

  public static void assertGame(Game game) {
    assertThat(game, notNullValue());

    Grid yourGrid = game.getYourGrid();
    Grid opponentGrid = game.getOpponentGrid();

    assertGrid(yourGrid);
    assertGrid(opponentGrid);
    assertThat(yourGrid.getPlayerNum(), anyOf(equalTo(1), equalTo(2)));
    assertThat(opponentGrid.getPlayerNum(), anyOf(equalTo(1), equalTo(2)));
    assertThat(game.getWinningPlayerNum(), nullValue());
  }

  public static void assertErrorMessage(GraphQLError error, String expectedMessage) {
    assertThat(error.getMessage(), stringContainsInOrder(expectedMessage));
  }

  public static void assertIsShipAdjacentToOccupiedCoordinates(
      Coordinate shipCoordinate, Coordinate occupiedCoordinate, boolean expectedResult) {
    Set<Coordinate> shipCoordinates = new HashSet<>();
    shipCoordinates.add(shipCoordinate);

    Set<Coordinate> occupiedCoordinates = new HashSet<>();
    occupiedCoordinates.add(occupiedCoordinate);

    assertThat(
        GridUtil.isShipAdjacentToOccupiedCoordinates(shipCoordinates, occupiedCoordinates),
        equalTo(expectedResult));
  }
}
