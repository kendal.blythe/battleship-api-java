package org.kblythe.battleship.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import java.net.URL;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.l10n.ErrorKeys;
import org.kblythe.battleship.l10n.Errors;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.model.GridConfig;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.GraphQLTestHelper;
import org.kblythe.battleship.testHelper.TestGraphQLResponseMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import graphql.GraphQLError;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ConfigApiTest {
  static final Resource RESOURCE_QUERY_GRID_CONFIGS =
      new ClassPathResource("graphql/query.gridConfigs.gql");
  static final Resource RESOURCE_QUERY_GRID_CONFIG =
      new ClassPathResource("graphql/query.gridConfig.gql");
  static final Resource RESOURCE_MUTATION_CREATE_GRID =
      new ClassPathResource("graphql/mutation.createGrid.gql");
  static final String INVALID_GRID_CONFIG_ID = "-";

  String url;
  @LocalServerPort int port;
  @Autowired TestRestTemplate restTemplate;

  @BeforeEach
  void beforeEach() throws Exception {
    url = new URL(String.format("http://localhost:%d/battleship/graphql", port)).toString();
  }

  @Test
  void testGetGridConfigs() {
    // given
    String queryString = GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIGS);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.query(restTemplate, url, queryString);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    List<GridConfig> gridConfigs =
        GraphQLTestHelper.getData(
            response, "gridConfigs", new TypeReference<List<GridConfig>>() {});
    List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

    AssertTestHelper.assertGridConfigSummaries(gridConfigs);
    assertThat(errors, nullValue());
  }

  @Test
  void testGetGridConfig() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // given
      String queryString =
          GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIG, gridConfigId);

      // when
      ResponseEntity<TestGraphQLResponseMap> response =
          GraphQLTestHelper.query(restTemplate, url, queryString);

      // then
      assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

      GridConfig gridConfig = GraphQLTestHelper.getData(response, "gridConfig", GridConfig.class);
      List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

      AssertTestHelper.assertGridConfig(gridConfig);
      assertThat(errors, nullValue());
    }
  }

  @Test
  void testGetGridConfigNotFound() {
    // given
    String queryString =
        GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIG, INVALID_GRID_CONFIG_ID);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.query(restTemplate, url, queryString);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    GridConfig gridConfig = GraphQLTestHelper.getData(response, "gridConfig", GridConfig.class);
    List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

    assertThat(gridConfig, nullValue());
    assertThat(errors, hasSize(1));
    AssertTestHelper.assertErrorMessage(
        errors.get(0), Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID));
  }

  @Test
  void testCreateGrid() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // given
      String mutationString =
          GraphQLTestHelper.getMutationString(RESOURCE_MUTATION_CREATE_GRID, gridConfigId);

      // when
      ResponseEntity<TestGraphQLResponseMap> response =
          GraphQLTestHelper.mutation(restTemplate, url, mutationString);

      // then
      assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

      Grid grid = GraphQLTestHelper.getData(response, "createGrid", Grid.class);
      List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

      AssertTestHelper.assertGrid(grid);
      assertThat(errors, nullValue());
    }
  }

  @Test
  void testCreateGridNotFound() {
    // given
    String mutationString =
        GraphQLTestHelper.getMutationString(RESOURCE_MUTATION_CREATE_GRID, INVALID_GRID_CONFIG_ID);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.mutation(restTemplate, url, mutationString);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    Grid grid = GraphQLTestHelper.getData(response, "createGrid", Grid.class);
    List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

    assertThat(grid, nullValue());
    assertThat(errors, hasSize(1));
    AssertTestHelper.assertErrorMessage(
        errors.get(0), Errors.getMessage(ErrorKeys.GRID_CONFIG_NOT_FOUND, INVALID_GRID_CONFIG_ID));
  }

  @Test
  void testMultipleQueries() {
    // given
    String firstGridConfigId = ConfigTestHelper.getGridConfigIds().iterator().next();
    String queryString1 = GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIGS);
    String queryString2 =
        GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIG, firstGridConfigId);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.query(restTemplate, url, queryString1, queryString2);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    List<GridConfig> gridConfigs =
        GraphQLTestHelper.getData(
            response, "gridConfigs", new TypeReference<List<GridConfig>>() {});
    GridConfig gridConfig = GraphQLTestHelper.getData(response, "gridConfig", GridConfig.class);
    List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

    AssertTestHelper.assertGridConfigSummaries(gridConfigs);
    AssertTestHelper.assertGridConfig(gridConfig);

    assertThat(errors, nullValue());
  }

  @Test
  void testInvalidQueryNameError() {
    // given
    String queryString = "X" + GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIGS);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.query(restTemplate, url, queryString);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    List<GridConfig> gridConfigs =
        GraphQLTestHelper.getData(
            response, "gridConfigs", new TypeReference<List<GridConfig>>() {});
    List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

    assertThat(gridConfigs, nullValue());
    assertThat(errors, hasSize(1));
  }

  @Test
  void testInvalidUrlError() {
    // given
    String queryString = GraphQLTestHelper.getQueryString(RESOURCE_QUERY_GRID_CONFIGS);

    // when
    ResponseEntity<TestGraphQLResponseMap> response =
        GraphQLTestHelper.query(restTemplate, url + "X", queryString);

    // then
    assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
  }
}
