package org.kblythe.battleship.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import java.net.URL;
import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kblythe.battleship.model.Coordinate;
import org.kblythe.battleship.model.Game;
import org.kblythe.battleship.model.Grid;
import org.kblythe.battleship.service.ConfigService;
import org.kblythe.battleship.testHelper.AssertTestHelper;
import org.kblythe.battleship.testHelper.ConfigTestHelper;
import org.kblythe.battleship.testHelper.GraphQLTestHelper;
import org.kblythe.battleship.testHelper.TestGraphQLResponseMap;
import org.kblythe.battleship.util.GameUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import graphql.GraphQLError;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class GameApiTest {
  static final Resource RESOURCE_MUTATION_START_GAME =
      new ClassPathResource("graphql/mutation.startGame.gql");
  static final Resource RESOURCE_MUTATION_DROP_BOMB =
      new ClassPathResource("graphql/mutation.dropBomb.gql");

  ConfigService configService = new ConfigService();
  String url;
  @LocalServerPort int port;
  @Autowired TestRestTemplate restTemplate;
  Random random;

  @BeforeEach
  void beforeEach() throws Exception {
    url = new URL(String.format("http://localhost:%d/battleship/graphql", port)).toString();
    random = new Random();
  }

  @Test
  void testPlayGame() {
    for (String gridConfigId : ConfigTestHelper.getGridConfigIds()) {
      // ========== start game ==========

      // given
      Grid grid = configService.createGrid(gridConfigId);
      String mutationString =
          GraphQLTestHelper.getMutationString(RESOURCE_MUTATION_START_GAME, grid);
      mutationString =
          mutationString.replace(
              ",playerId:null,playerNum:0,systemPlayer:false,bombedCoordinates:[]",
              ""); // remove non-existent grid input properties
      mutationString =
          mutationString.replace("id:null,", ""); // remove non-existent ship input properties

      // when
      ResponseEntity<TestGraphQLResponseMap> response =
          GraphQLTestHelper.mutation(restTemplate, url, mutationString);

      // then
      assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

      Game game = GraphQLTestHelper.getData(response, "startGame", Game.class);
      List<GraphQLError> errors = GraphQLTestHelper.getErrors(response);

      AssertTestHelper.assertGame(game);
      assertThat(errors, nullValue());

      // ========== drop bombs until game over ==========

      while (game.getWinningPlayerNum() == null) {
        // given
        Coordinate coordinate =
            GameUtil.getRandomAvailableCoordinate(game.getOpponentGrid(), random);
        mutationString =
            GraphQLTestHelper.getMutationString(
                RESOURCE_MUTATION_DROP_BOMB,
                game.getId(),
                game.getYourGrid().getPlayerId(),
                coordinate);

        // when
        response = GraphQLTestHelper.mutation(restTemplate, url, mutationString);

        // then
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        game = GraphQLTestHelper.getData(response, "dropBomb", Game.class);
        errors = GraphQLTestHelper.getErrors(response);

        assertThat(game, notNullValue());
        assertThat(errors, nullValue());
      }

      // then
      assertThat(game.getWinningPlayerNum(), anyOf(equalTo(1), equalTo(2)));
    }
  }
}
