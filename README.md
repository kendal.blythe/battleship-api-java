# Battleship API (Java)

## Introduction

<a href="https://en.wikipedia.org/wiki/Battleship_(game)">Battleship</a> is a classic strategy/guessing game of naval combat. This project implements a <a href="https://graphql.org/">GraphQL</a> API written in <a href="https://www.java.com/">Java</a> for playing Battleship. See the corresponding <a href="https://gitlab.com/kendal.blythe/battleship-ui-react">Battleship UI (React)</a> project for the web application.

## Technologies

- <a href="https://www.java.com/">Java</a>
- <a href="https://spring.io/projects/spring-boot">Spring Boot</a>
- <a href="https://spring.io/projects/spring-data-jpa">Spring Data JPA</a>
- <a href="https://graphql.org/">GraphQL</a>
- <a href="https://www.postgresql.org/">PostgreSQL</a>
- <a href="https://www.h2database.com/">H2 Database</a>
- <a href="https://www.liquibase.org/">Liquibase</a>
- <a href="https://junit.org/">JUnit</a>
- <a href="https://site.mockito.org/">Mockito</a>
- <a href="https://powermock.github.io/">PowerMock</a>
- <a href="https://github.com/OpenPojo/openpojo">OpenPojo</a>
- <a href="http://hamcrest.org/JavaHamcrest/">Hamcrest</a>
- <a href="https://www.eclipse.org/">Eclipse</a>
- <a href="https://github.com/google/google-java-format">Google Java Format</a>
- <a href="https://gradle.org/">Gradle</a>
- <a href="https://gitlab.com/">GitLab</a>

## Play Battleship

You can play a game of Battleship here.

- <a href="https://battleship-ui-react.herokuapp.com/">https://battleship-ui-react.herokuapp.com/</a>

Be patient while the application loads the first time! Both Battleship projects are automatically deployed to <a href="https://www.heroku.com/">Heroku Cloud Platform</a> (free tier) using GitLab CI/CD.

## Getting Started

After you clone the Git repository, you can run the following <a href="https://gradle.org/">Gradle</a> commands.
- `./gradlew build` - build project
- `./gradlew test` - run unit tests
- `./gradlew integrationTest` - run integration tests
- `./gradlew bootRun` - start server

## Testing

One of the goals for this project was to achieve 100% code coverage. Here are the results!

```
Element                          Coverage  Covered  Missed
src/main/java                      100.0%    3,259       0
org.kblythe.battleship             100.0%        8       0
org.kblythe.battleship.entity      100.0%      435       0
org.kblythe.battleship.exception   100.0%      234       0
org.kblythe.battleship.graphql     100.0%      185       0
org.kblythe.battleship.l10n        100.0%       31       0
org.kblythe.battleship.model       100.0%      374       0
org.kblythe.battleship.service     100.0%      953       0
org.kblythe.battleship.util        100.0%      977       0
org.kblythe.battleship.web         100.0%       62       0
```

- Unit Test Results: SUCCESS (68 tests, 68 successes, 0 failures, 0 skipped)
- Integration Test Results: SUCCESS (9 tests, 9 successes, 0 failures, 0 skipped)

:smile:

## Developer

<a href="https://www.linkedin.com/in/kendal-blythe/">Kendal Blythe</a> - Check me out on LinkedIn!
